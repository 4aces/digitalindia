(function() {
    'use restrict';

    function districtService(Restangular) {
        var factories = {};

        var District = Restangular.all('district');

        var get = function(params) {
            return District
                .customGET(null, params);
        };

        var create = function(params) {
            return District.post(params);
        };

        var getById = function(id) {
            return Restangular
                .one('district', id)
                .get();
        };

        var updateById = function(id, obj) {
            return Restangular
                .one('district', id)
                .customPUT(obj);
        };

        factories.get = get;
        factories.create = create;
        factories.getById = getById;
        factories.updateById = updateById;

        return factories;
    }

    angular
        .module('app.core')
        .factory('districtService', ['Restangular', districtService]);

})();
