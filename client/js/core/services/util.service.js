(function() {

    'use strict'

    function UtilService() {
        var factories = {};
        var graphSetting = {
            colors: ['#1b9e77', '#d95f02', '#7570b3'],
            height: 250,
            tooltip: { isHtml: true }
        };

        function convertToGraph(result) {

            var graph = {};
            graph.data = result.Data;
            graph.type = result.Type;
            graph.options = merge_options(result.ChartOptions, graphSetting);
            return graph;
        }

        function merge_options(obj1, obj2) {
            var obj3 = {};
            for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
            for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
            return obj3;
        }

        function formatDate(date) {
            return moment(date).format('DD-MMM-YYYY');
        }

        factories.convertToGraph = convertToGraph;
        factories.merge_options = merge_options;
        factories.formatDate = formatDate;

        return factories;
    }

    angular
        .module('app.core')
        .factory('UtilService', [UtilService]);


})();
