(function() {

    'user strict';


    function FileuploadService(Restangular) {
        var factories = {};
        var postFile = function(params)
        {

                var fd = new FormData();
                fd.append('photo', params);
                //fd.append('docType',1);
            return Restangular
                .all('api/Files')
                /*.post(fd,[{ photo: params},{ 'Content-Type': application/octet-stream}]) */
                .customPOST(fd, '', undefined, {'Content-Type': undefined})
                .then(function(response){
                    console.log(response);
                })
               
        }

        var deleteFile = function(params)
        {
            return Restangular
               .all('api/Files?fileName='+params)
               .customDELETE('',[{params},{'Content-Type':undefined}])
        }                            

        factories.postFile= postFile;
        factories.deleteFile=deleteFile;
        return factories;
    }

    angular
        .module('app.core')
        .factory('FileuploadService', ['Restangular', FileuploadService]);

})();
