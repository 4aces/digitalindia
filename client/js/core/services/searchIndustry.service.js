(function() {
    'use restrict';

    function Service(Restangular) {
        var factories = {};

        var Industry = Restangular.all('search-industries');


        //console.log(Crop);

        var get = function(params) {
            return Industry.customGET(null, params);
        };

        factories.get = get;

        return factories;
    }

    angular
        .module('app.core')
        .factory('searchIndustryService', ['Restangular', Service]);

})();
