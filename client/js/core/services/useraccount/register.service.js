(function() {
    'use restrict';

    function RegisterService(Restangular) {
        var factories = {};

        var newRegister = function(params) {
            return Restangular
                .all('api/Account/Register')
                .post(params)
                .then(function(response) {
                    console.log(response)
                });
        };

        factories.newRegister = newRegister;
        return factories;
    }

    angular
        .module('app.core')
        .factory('RegisterService', ['Restangular', RegisterService]);

})();
