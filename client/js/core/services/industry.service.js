(function() {
    'use restrict';

    function Service(Restangular) {
        var factories = {};

        var Industry = Restangular.all('industry');


        //console.log(Crop);

        var get = function(params) {
            return Industry.customGET(null, params);
        };
        var create = function(params) {
            return Industry.post(params);
        };

        var getById = function(id) {
            return Restangular
                .one('industry', id)
                .get();
        };

        var updateById = function(id, obj) {
            return Restangular
                .one('industry', id)
                .customPUT(obj);
        };

        factories.get = get;
        factories.create = create;
        factories.getById = getById;
        factories.updateById = updateById;
        return factories;
    }

    angular
        .module('app.core')
        .factory('industryService', ['Restangular', Service]);

})();
