(function() {
    'use restrict';

    function cropService(Restangular) {
        var factories = {};

        var Crop = Restangular.all('crops');


        //console.log(Crop);

        var getCrops = function(params) {
            return Crop.customGET(null,params);
        };
        var getCropById = function(id) {
            return Restangular
                .one('crops', id)
                .get();
        };

        var updateCropById = function(id, obj) {
            return Restangular
                .one('crops', id)
                .customPUT(obj);
        };

        factories.getCrops = getCrops;
        factories.getCropById = getCropById;
        factories.updateCropById = updateCropById;
        return factories;
    }

    angular
        .module('app.core')
        .factory('cropService', ['Restangular', cropService]);

})();
