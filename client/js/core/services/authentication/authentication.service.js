/**
 * Authentication service.
 *
 * @author    Martin Micunda {@link http://martinmicunda.com}
 * @copyright Copyright (c) 2015, Martin Micunda
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
(function() {
    'use strict';

    /**
     * @ngInject
     */
    function AuthenticationProvider() {
        this.$get = function($http, Restangular, Token, localStorageService, $q) {
            var currentUser = null;

            function saveUserAndToken(tokenAndUser) {
                // store token to local storage
                Token.set(tokenAndUser.access_token);

                // save user to locale storage
                localStorageService.set('roles', tokenAndUser.roles.toString());
                localStorageService.set('username', tokenAndUser.userName);


                var roles = tokenAndUser.roles.split(',');
                var filterAdmin = roles.filter(function(role) {
                    if (role == "Admin")
                        return true;
                    else
                        return false;
                });

                localStorageService.set('isAdmin', filterAdmin.length > 0 ? true : false);

            }

            return {
                signup: function(params) {
                    return Restangular
                        .all('auth/signup')
                        .post(params)
                        .then(function(response) {
                            saveUserAndToken(response.token);
                        });
                },
                signin: function(params) {

                    Token.remove();

                    var data = "grant_type=password&username=" + params.username + "&password=" + params.password;

                    return Restangular
                        .all('Token')
                        .post(data, {}, { 'Content-Type': 'application/x-www-form-urlencoded;' })
                        .then(function(response) {
                            saveUserAndToken(response);
                        });
                },
                signout: function() {

                    return Restangular
                        .one('api/Account/Logout')
                        .post()
                        .then(function() {
                            // currentUser = null;
                            Token.remove();
                        });
                },

                isAdmin: function() {
                    return localStorageService.get('isAdmin');
                },

                isAuthenticated: function() {
                    return !!Token.get();
                },
                getCurrentUser: function() {
                    return currentUser || localStorageService.get('user')
                }
            };
        };
    }

    angular
        .module('app.core')
        .provider('Authentication', AuthenticationProvider);
})();
