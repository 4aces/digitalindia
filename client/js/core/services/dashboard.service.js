(function() {
    'use restrict';

    function dashboardService(Restangular) {
        var factories = {};

        var Dashboard = Restangular.all('dashboard');

        var getDistrictCropByYear = function(params) {
            return Dashboard
                .all('district-crop-by-year')
                .customGET(null, params);
        };

        var getDistrictCropYeildByYear = function(params) {
            return Dashboard
                .all('district-crop-yeild-by-year')
                .customGET(null, params);
        };

        var getCropDistrictYeildByYear = function(params){
            return Dashboard
                .all('crop-district-yeild-by-year')
                .customGET(null, params);
        }

        factories.getDistrictCropByYear = getDistrictCropByYear;
        factories.getDistrictCropYeildByYear = getDistrictCropYeildByYear;
        factories.getCropDistrictYeildByYear = getCropDistrictYeildByYear;

        return factories;
    }

    angular
        .module('app.core')
        .factory('dashboardService', ['Restangular', dashboardService]);

})();
