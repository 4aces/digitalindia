(function() {
    'use restrict';

    function masterService(Restangular) {
        var factories = {};

        var Master = Restangular.all('master');
        var getCrops = function() {
            return Master
                .all('crops')
                .getList();
        };

        var getIndustries = function() {
            return Master
                .all('industries')
                .getList();
        };

        var getDistricts = function() {
            return Master
                .all('districts')
                .getList();
        };

        var getStates = function() {
            return Master
                .all('states')
                .getList();
        }

        var getDistrictsByState = function(id) {
            return Master
                .all('state/' + id + '/districts')
                .getList();
        }
        var getCropsByDistrict = function(id) {
            return Master
                .all('district/' + id + '/crops')
                .getList();
        }

        var getYears = function() {


            var years = [];

            for (var i = 1998; i <= 2010; i++) {
                var obj = {};

                obj.key = i.toString();
                obj.value = i.toString();

                years.push(obj);

            }

            return years;

        }

        factories.getCrops = getCrops;
        factories.getIndustries = getIndustries;
        factories.getDistricts = getDistricts;
        factories.getStates = getStates;
        factories.getDistrictsByState = getDistrictsByState;
        factories.getCropsByDistrict = getCropsByDistrict;
        factories.getYears = getYears;


        return factories;
    }

    angular
        .module('app.core')
        .factory('masterService', ['Restangular', masterService]);

})();
