﻿/**
 * Signin route.
 *
 */
(function() {
    'use strict';

    /**
     * @ngdoc object
     * @name signinRoute
     * @module app.signin
     * @requires $stateProvider
     * @description
     * Router for the signin page.
     *
     * @ngInject
     */
    function CropInboxRoute($stateProvider) {
        $stateProvider
            .state('admin.cropinbox', {
                url: '/crop/inbox',
                views: {
                    adminContent: {
                        templateUrl: '/js/routes/crop/inbox/inbox.html',
                        controller: 'CropInboxCtrl as vm'
                    }
                },
                resolve: {
                    cropMater: ['masterService', function(masterService) {
                        return masterService.getCrops();
                    }]
                },
                data: {
                    authenticate: false
                }
            });
    }

    angular
        .module('app.crop')
        .config(['$stateProvider', CropInboxRoute]);
})();
