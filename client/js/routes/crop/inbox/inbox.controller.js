﻿/**
 * SignIn controller.
 *
 */
(function() {
    'use strict';

    function CropInboxCtrl($mdMedia, $mdDialog, cropService, cropMater) {

        console.log(cropMater);
        var vm = this;


        // list of `state` value/display objects
        vm.cropMaster = cropMater;
        vm.queryCropSearch = queryCropSearch;
        vm.selectedCropChange = selectedCropChange;
        vm.searchCropChange = searchCropChange;


        vm.query = {
            order: '-Name',
            limit: 10,
            page: 1,
            q: ''
        };

        vm.getCrops = function() {
            console.log(vm.query);
            vm.promise = cropService.getCrops(vm.query).then(
                function(response) {
                    console.log(response);
                    vm.crops = response;
                    return response;
                },
                function(err) {
                    console.log(err);
                    return err;
                });
        };
        vm.getCrops();

        vm.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

        vm.openEdit = function(ev, id) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && vm.customFullscreen;

            $mdDialog.show({
                    controller: 'CropEditCtrl as vm',
                    templateUrl: 'js/routes/crop/edit/edit.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: useFullScreen,
                    resolve: {
                        crop: ['cropService', function(cropService) {
                            return cropService.getCropById(id)
                        }]
                    }
                })
                .then(function(answer) {
                    //$scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    //$scope.status = 'You cancelled the dialog.';
                });
        };
        /*$http.get('http://vocab.nic.in/rest.php/states/json').then(function(response) {
            console.log(response);
        });*/


        function queryCropSearch(query) {
            var results = query ? vm.cropMaster.filter(createFilterFor(query)) : vm.cropMaster;
            return results;
        }

        function searchCropChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedCropChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));
            if (item) {
                vm.query.q = item.value;
            }else
            {
                vm.query.q='';
            }
            vm.getCrops();
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            console.log(query);
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(crop) {
                //console.log(crop);
                //console.log(lowercaseQuery);
                return (crop.value.toLowerCase().indexOf(lowercaseQuery) === 0);
            };
        }

    }
    angular
        .module('app.crop')
        .controller('CropInboxCtrl', ['$mdMedia', '$mdDialog', 'cropService', 'cropMater', CropInboxCtrl]);

})();
