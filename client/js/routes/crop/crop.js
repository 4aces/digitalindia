﻿/**
 * SignIn module.
 *
 */
(function () {
    'use strict';

    /**
     * @ngdoc module
     * @name app.crop
     */
    angular.module('app.crop', []);
})();
