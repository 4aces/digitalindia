  (function() {
      'use strict';

      function CropEditCtrl($mdDialog, $window, crop, cropService) {

          console.log(crop);
          var vm = this;
          vm.editDisabled = false;
          vm.showProgress = false;
          vm.isSuccess = false;
          vm.crop = crop;
          vm.msgShow = false;
          vm.cancel = function() {
              $mdDialog.cancel();
              $window.close();
          };

          vm.update = function() {
              vm.editDisabled = true;
              vm.showProgress = true;
              cropService.updateCropById(vm.crop._id, vm.crop).then(function(response) {
                  console.log(response);
                  vm.isSuccess = true;
                  vm.showProgress = false;
                  vm.msgShow = true;
                  vm.responseMsg = "Sucessfully edited";
              }, function(err) {
                  vm.editDisabled = false;
                  vm.msgShow = true;
                  console.log(err);
              });
          };
          vm.forgotPassword = function() {
              vm.Msgshow = true;
              vm.Confirmmsg = "Password Reset Link has been sent to your EmailID !";
              //$mdDialog.hide(true);
              //$state.go('signin');
              console.log(vm.Confirmmsg);
              console.log(vm.message);

          };
      }

      angular
          .module('app.crop')
          .controller('CropEditCtrl', ['$mdDialog', '$window', 'crop', 'cropService', CropEditCtrl]);
  })();
