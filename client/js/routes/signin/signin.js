﻿/**
 * SignIn module.
 *
 */
(function () {
    'use strict';

    /**
     * @ngdoc module
     * @name app.query
     */
    angular.module('app.signin', []);
})();
