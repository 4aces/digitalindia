﻿/**
 * SignIn controller.
 *
 */
(function() {
    /**
     * @ngdoc controller
     * @name SigninCtrl
     * @module app.signin
     * @requires $rootScope
     * @requires $state
     * @requires Authentication
     * @description
     * Controller for the signin page.
     *
     * @ngInject
     */
    function SigninCtrlType($mdMedia,$mdDialog,$rootScope,$state,Authentication,$timeout) {
        var vm = this;
        vm.Loginfailed = false;
        vm.signIn = function(credentials, isValid) {
            console.log(credentials);
            if(!isValid) {return;}
            Authentication.signin(credentials).then(function () {
              //  save user profile details to $rootScope
               $rootScope.me = Authentication.getCurrentUser();

               $state.go('provider.inbox');
            }, function(error) {
                vm.Errormsg="User name or password is incorrect";
                vm.Loginfailed= true;
                  $timeout(function () {
                      vm.Loginfailed= false;
                     }, 8000);
                console.log(error);
            });
        };

         vm.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

         vm.openForgotPassword = function(ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && vm.customFullscreen;

            $mdDialog.show({
                    controller: SigninCtrl,
                    templateUrl: 'js/routes/signin/forgot-Password.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: useFullScreen
                })
                .then(function(answer) {
                    //$scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    //$scope.status = 'You cancelled the dialog.';
                });
        };

    }

    function SigninCtrl($mdDialog,$scope,$state,$window){
        $scope.Msgshow=false;
        var vm=this;
        $scope.cancel = function(){
             $mdDialog.cancel();
             $window.close()
        };
        $scope.forgotPassword = function(){
            $scope.Msgshow=true;
            $scope.Confirmmsg="Password Reset Link has been sent to your EmailID !";
            //$mdDialog.hide(true);
            //$state.go('signin');
            console.log($scope.Confirmmsg);
            console.log(vm.message);

        };

    }

    angular
        .module('app.signin')
        .controller('SigninCtrlType',[ '$mdMedia','$mdDialog','$rootScope','$state','Authentication','$timeout',SigninCtrlType]);
    
})();