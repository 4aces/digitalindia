﻿/**
 * Signin route.
 *
 */
(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name signinRoute
     * @module app.signin
     * @requires $stateProvider
     * @description
     * Router for the signin page.
     *
     * @ngInject
     */
    function signinRoute($stateProvider) {
        $stateProvider
            .state('signin', {
                url: '/signin',
                templateUrl: '/js/routes/signin/signin.html',
                controller: 'SigninCtrlType as vm',
                data: {
                    authenticate: false
                }
            });
    }
    
    angular
        .module('app.signin')
        .config(['$stateProvider',signinRoute]);
})();
