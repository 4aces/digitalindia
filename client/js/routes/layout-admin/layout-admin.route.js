(function() {
    'use strict';

    function LayoutRoute($stateProvider) {
        $stateProvider.state('admin', {
            url: '',
            abstract: true,
            templateUrl: '/js/routes/layout-admin/layout-admin.html',
            controller: 'LayoutAdminCtrl as vm'
        });
    }

    angular.module('app.layoutAdmin')
        .config(['$stateProvider', LayoutRoute]);
})();
