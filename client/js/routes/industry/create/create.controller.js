  (function() {
      'use strict';

      function IndustryCtrl($mdDialog, $window, crop, industryService) {

          console.log(crop);
          var vm = this;

          vm.cropMaster = crop;
          vm.queryCropSearch = queryCropSearch;
          vm.selectedCropChange = selectedCropChange;
          vm.searchCropChange = searchCropChange;
          vm.transformChip = transformChip;

          vm.submitDisabled = false;
          vm.showProgress = false;
          vm.isSuccess = false;
          vm.msgShow = false;
          vm.cancel = function() {
              $mdDialog.cancel();
              $window.close();
          };

          vm.selectedCrops = [];


          vm.create = function(industry) {

            industry.CropIds=[];

            vm.selectedCrops.forEach(function(value){
              industry.CropIds.push(value.key);
            });

              vm.submitDisabled = true;
              vm.showProgress = true;
              industryService.create(industry).then(function(response) {
                  console.log(response);
                  vm.isSuccess = true;
                  vm.showProgress = false;
                  vm.msgShow = true;
                  vm.responseMsg = "Sucessfully added";
              }, function(err) {
                  vm.submitDisabled = false;
                  vm.msgShow = true;
                  vm.responseMsg = err.join(',');
                  console.log(err);
              });
          };

          function transformChip(chip) {
              // If it is an object, it's already a known chip
              if (angular.isObject(chip)) {
                  return chip;
              }

              // Otherwise, create a new one
              //return { name: chip, type: 'new' }
          }

          function queryCropSearch(query) {
              var results = query ? vm.cropMaster.filter(createFilterFor(query)) : vm.cropMaster;
              return results;
          }

          function searchCropChange(text) {
              console.log('Text changed to ' + text);
          }

          function selectedCropChange(item) {
              console.log('Item changed to ' + JSON.stringify(item));

          }

          /**
           * Create filter function for a query string
           */
          function createFilterFor(query) {
              console.log(query);
              var lowercaseQuery = angular.lowercase(query);
              return function filterFn(crop) {
                  //console.log(crop);
                  //console.log(lowercaseQuery);
                  return (crop.value.toLowerCase().indexOf(lowercaseQuery) === 0);
              };
          }

      }

      angular
          .module('app.industry')
          .controller('IndustryCreateCtrl', ['$mdDialog', '$window', 'crop', 'industryService', IndustryCtrl]);
  })();
