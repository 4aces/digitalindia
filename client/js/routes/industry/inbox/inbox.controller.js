/**
 * SignIn controller.
 *
 */
(function() {
    'use strict';

    function InboxCtrl($mdMedia, $mdDialog, industryService, industryMater) {

        //console.log(IndustryMater);
        var vm = this;


        // list of `state` value/display objects
        vm.IndustryMaster = industryMater;
        vm.queryIndustrySearch = queryIndustrySearch;
        vm.selectedIndustryChange = selectedIndustryChange;
        vm.searchIndustryChange = searchIndustryChange;


        vm.query = {
            order: '-Name',
            limit: 10,
            page: 1,
            q: ''
        };

        vm.getIndustries = function() {
            console.log(vm.query);
            vm.promise = industryService.get(vm.query).then(
                function(response) {
                    console.log(response);
                    vm.industries = response;
                    return response;
                },
                function(err) {
                    console.log(err);
                    return err;
                });
        };
        vm.getIndustries();

        vm.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

         vm.openCreate = function(ev) {

            

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && vm.customFullscreen;

            $mdDialog.show({
                    controller: 'IndustryCreateCtrl as vm',
                    templateUrl: 'js/routes/industry/create/create.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: useFullScreen,
                    resolve: {
                        crop: ['masterService', function(masterService) {
                            return masterService.getCrops();
                        }]
                    }
                })
                .then(function(answer) {
                    vm.getIndustries();
                    //$scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    vm.getIndustries();
                    //$scope.status = 'You cancelled the dialog.';
                });
        };
       

        vm.openEdit = function(ev, id) {

            console.log('industry');
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && vm.customFullscreen;

            $mdDialog.show({
                    controller: 'IndustryEditCtrl as vm',
                    templateUrl: 'js/routes/industry/edit/edit.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: useFullScreen,
                    resolve: {
                        industry: ['industryService', function(industryService) {
                            return industryService.getById(id)
                        }], 
                        crop: ['masterService', function(masterService) {
                            return masterService.getCrops();
                        }]
                    }
                })
                .then(function(answer) {
                    //$scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    //$scope.status = 'You cancelled the dialog.';
                });
        };
        /*$http.get('http://vocab.nic.in/rest.php/states/json').then(function(response) {
            console.log(response);
        });*/


        function queryIndustrySearch(query) {
            var results = query ? vm.IndustryMaster.filter(createFilterFor(query)) : vm.IndustryMaster;
            return results;
        }

        function searchIndustryChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedIndustryChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));
            if (item) {
                vm.query.q = item.value;
            } else {
                vm.query.q = '';
            }
            vm.getIndustries();
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            console.log(query);
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(Industry) {
                //console.log(Industry);
                //console.log(lowercaseQuery);
                return (Industry.value.toLowerCase().indexOf(lowercaseQuery) === 0);
            };
        }

    }
    angular
        .module('app.industry')
        .controller('IndustryInboxCtrl', ['$mdMedia', '$mdDialog', 'industryService', 'industryMater', InboxCtrl]);

})();
