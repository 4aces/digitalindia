﻿/**
 * Signin route.
 *
 */
(function() {
    'use strict';

    /**
     * @ngdoc object
     * @name signinRoute
     * @module app.signin
     * @requires $stateProvider
     * @description
     * Router for the signin page.
     *
     * @ngInject
     */
    function InboxRoute($stateProvider) {
        $stateProvider
            .state('admin.industryinbox', {
                url: '/industry/inbox',
                views: {
                    adminContent: {
                        templateUrl: '/js/routes/industry/inbox/inbox.html',
                        controller: 'IndustryInboxCtrl as vm'
                    }
                },
                resolve: {
                    industryMater: ['masterService', function(masterService) {
                        return masterService.getIndustries();
                    }]
                },
                data: {
                    authenticate: false
                }
            });
    }

    angular
        .module('app.industry')
        .config(['$stateProvider', InboxRoute]);
})();
