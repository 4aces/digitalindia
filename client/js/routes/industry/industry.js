﻿/**
 * Industry module.
 *
 */
(function () {
    'use strict';

    /**
     * @ngdoc module
     * @name app.industry
     */
    angular.module('app.industry', []);
})();
