﻿/**
 * Signin route.
 *
 */
(function() {
    'use strict';

    /**
     * @ngdoc object
     * @name signinRoute
     * @module app.signin
     * @requires $stateProvider
     * @description
     * Router for the signin page.
     *
     * @ngInject
     */
    function districtCropRoute($stateProvider) {
        $stateProvider
            .state('admin.districtCrop', {
                url: '/districtCrop',
                views: {
                    adminContent: {
                        templateUrl: '/js/routes/district-crop/district-crop.html',
                        controller: 'DistrictCropCtrl as vm'
                    }
                },
                data: {
                    authenticate: false
                }
            });
    }

    angular
        .module('app.districtCrop')
        .config(['$stateProvider', districtCropRoute]);
})();
