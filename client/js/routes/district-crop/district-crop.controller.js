﻿/**
 * SignIn controller.
 *
 */
(function() {
    'use strict';

    function DistrictCropCtrl(districtService, $http) {

        var vm = this;
        vm.query = {
            order: '-Year',
            limit: 25,
            page: 1
        };

        vm.getDistrictCrop = function() {
            console.log(vm.query);
            vm.promise = districtService.getDistrict(vm.query).then(
                function(response) {
                    console.log(response);
                    vm.districtCrop = response;
                    return response;
                },
                function(err) {
                    console.log(err);
                    return err;
                });
        };
        vm.getDistrictCrop();

        $http.get('http://vocab.nic.in/rest.php/states/json').then(function(response) {
            console.log(response);
        });

    }
    angular
        .module('app.districtCrop')
        .controller('DistrictCropCtrl', ['districtService', '$http', DistrictCropCtrl]);

})();
