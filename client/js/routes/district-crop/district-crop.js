﻿/**
 * SignIn module.
 *
 */
(function () {
    'use strict';

    /**
     * @ngdoc module
     * @name app.districtCrop
     */
    angular.module('app.districtCrop', []);
})();
