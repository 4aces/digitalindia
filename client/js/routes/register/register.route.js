/**
 * User route.
 *
 */
(function() {
    'use strict';

    /**
     * @ngdoc object
     * @name userRoute
     * @module app.query
     * @requires $stateProvider
     * @description
     * Router for the query page.
     *
     * @ngInject
     */
    function registerRoute($stateProvider) {
        $stateProvider
            .state('provider.register', {
                url: '/register',
                views:{
                    providerContent:{
                          templateUrl: '/js/routes/register/register.html',
                          controller: 'RegisterCtrl as vm',
                    }
                },
                data: {
                    authenticate: false
                }
            });
              
}
    angular
        .module('app.register')
        .config(['$stateProvider', registerRoute]);

})();
