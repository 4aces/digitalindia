  (function() {
      'use strict';

      function CropDialogCtrl($mdDialog, $window, items) {

          console.log(items);
          var vm = this;
          vm.items = items;
          vm.cancel = function() {
              $mdDialog.cancel();
              $window.close();
          };
      }

      angular
          .module('app.searchIndustry')
          .controller('CropDialogCtrl', ['$mdDialog', '$window', 'items', CropDialogCtrl]);
  })();
