﻿/**
 * Signin route.
 *
 */
(function() {
    'use strict';

    /**
     * @ngdoc object
     * @name signinRoute
     * @module app.signin
     * @requires $stateProvider
     * @description
     * Router for the signin page.
     *
     * @ngInject
     */
    function Route($stateProvider) {
        $stateProvider
            .state('client.searchIndustry', {
                url: '/search-industry',
                views: {
                    clientContent: {
                        templateUrl: '/js/routes/client/search-industry/search-industry.html',
                        controller: 'SearchIndustryCtrl as vm'
                    }
                },
                resolve: {
                    stateMaster: ['masterService', function(masterService) {
                        return masterService.getStates().then(function(response) {
                            return response;
                        }, function(err) {
                            console.log(err)
                            return err;
                        });
                    }],
                    industries: ['searchIndustryService', function(searchIndustryService) {
                        return searchIndustryService.get().then(function(response) {
                            return response;
                        }, function(err) {
                            console.log(err)
                            return err;
                        });
                    }]
                },
                data: {
                    authenticate: false
                }
            });
    }

    angular
        .module('app.searchIndustry')
        .config(['$stateProvider', Route]);
})();
