﻿/**
 * SignIn controller.
 *
 */
(function() {
    'use strict';

    function SearchIndustryCtrl($mdMedia, $mdDialog, stateMaster, masterService, industries, searchIndustryService) {

        var vm = this;

        vm.StateMaster = stateMaster;
        vm.queryStateSearch = queryStateSearch;
        vm.selectedStateChange = selectedStateChange;
        vm.searchStateChange = searchStateChange;

        vm.industries = industries;

        vm.query = {
            order: 'Name',
            limit: 25,
            page: 1,
            state: '',
            district: '',
            crop: ''
        };

        vm.getIndustries = function() {
            vm.promise = searchIndustryService.get(vm.query).then(
                function(response) {
                    console.log(response);
                    vm.industries = response;
                    return response;
                },
                function(err) {
                    console.log(err);
                    return err;
                });
        }

        function queryStateSearch(query) {
            var results = query ? vm.StateMaster.filter(createFilterFor(query)) : vm.StateMaster;
            return results;
        }

        function searchStateChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedStateChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));
            vm.DistrictMaster = [];
            if (item === undefined) {
                console.log(vm.selectedState);
                console.log(vm.searchState);
                vm.query.state = '';
                vm.selectedDistrict = undefined;
                vm.searchDistrict = '';
            } else {
                vm.query.state = item.key;
                getDistricts(item.key);
            }
            vm.getIndustries();
        }


        vm.DistrictMaster = [];
        vm.queryDistrictSearch = queryDistrictSearch;
        vm.selectedDistrictChange = selectedDistrictChange;
        vm.searchDistrictChange = searchDistrictChange;

        function queryDistrictSearch(query) {
            var results = query ? vm.DistrictMaster.filter(createFilterFor(query)) : vm.DistrictMaster;
            return results;
        }

        function searchDistrictChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedDistrictChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));

            vm.cropMaster = [];
            if (item === undefined) {
                vm.query.district = '';

                vm.selectedCrop = undefined;
                vm.searchCrop = '';

            } else {
                vm.query.district = item.key;
                getCrops(item.key);
            }
            vm.getIndustries();


        }

        vm.cropMaster = [];
        vm.queryCropSearch = queryCropSearch;
        vm.selectedCropChange = selectedCropChange;
        vm.searchCropChange = searchCropChange;

        function queryCropSearch(query) {
            var results = query ? vm.cropMaster.filter(createFilterFor(query)) : vm.cropMaster;
            return results;
        }

        function searchCropChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedCropChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));
            if (item === undefined) {
                vm.query.crop = '';
            } else {
                vm.query.crop = item.key;
            }


            vm.getIndustries();


        }
        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            console.log(query);
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(Data) {
                //console.log(District);
                //console.log(lowercaseQuery);
                return (Data.value.toLowerCase().indexOf(lowercaseQuery) === 0);
            };
        }


        function getDistricts(id) {
            masterService.getDistrictsByState(id).then(
                function(response) {
                    vm.DistrictMaster = response;
                },
                function(err) {
                    console.log(err);
                });
        }

        function getCrops(id) {
            masterService.getCropsByDistrict(id).then(
                function(response) {
                    vm.cropMaster = response;
                },
                function(err) {
                    console.log(err);
                });
        }


        vm.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

        vm.openCropDialog = function(ev, crops) {

            console.log(crops);
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && vm.customFullscreen;

            $mdDialog.show({
                    controller: 'CropDialogCtrl as vm',
                    templateUrl: 'js/routes/client/search-industry/crop-dialog/dialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen,
                    locals: {
                        items: crops
                    }

                })
                .then(function(answer) {
                    //$scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    //$scope.status = 'You cancelled the dialog.';
                });
        };


    }
    angular
        .module('app.searchIndustry')
        .controller('SearchIndustryCtrl', ['$mdMedia', '$mdDialog', 'stateMaster', 'masterService', 'industries', 'searchIndustryService', SearchIndustryCtrl]);

})();
