﻿/**
 * Layout route.
 *
 */
(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name layoutRoute
     * @module app.layout
     * @requires $stateProvider
     * @description
     * Router for the layout page.
     *
     * @ngInject
     */
    function layoutRoute($stateProvider) {
        $stateProvider
            .state('client', {
                url: '',
                abstract: true,
                templateUrl: '/js/routes/client/layout/layout.html',
                controller: 'LayoutCtrl as vm'
            })
    }

    angular
        .module('app.layout')
        .config(['$stateProvider',layoutRoute]);
})();
