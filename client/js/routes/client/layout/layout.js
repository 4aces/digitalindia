﻿/**
 * 
 */
(function () {
    'use strict';

    /**
     * @ngdoc module
     * @name app.layout
     */
    angular.module('app.layout', []);

})();
