﻿/**
 * Signin route.
 *
 */
(function() {
    'use strict';

    /**
     * @ngdoc object
     * @name signinRoute
     * @module app.signin
     * @requires $stateProvider
     * @description
     * Router for the signin page.
     *
     * @ngInject
     */
    function districtCropYeildByYearRoute($stateProvider) {
        $stateProvider
            .state('client.districtCropYeildByYear', {
                url: '/dashboard/district-crop-yeild-by-year',
                views: {
                    clientContent: {
                        templateUrl: '/js/routes/client/dashboard/district-crop-yeild-by-year/district-crop-yeild-by-year.html',
                        controller: 'DistrictCropYeildByYearCtrl as vm'
                    }
                },
                resolve: {
                    stateMaster: ['masterService', function(masterService) {
                        return masterService.getStates().then(function(response) {
                            return response;
                        }, function(err) {
                            console.log(err)
                            return err;
                        });
                    }]
                },
                data: {
                    authenticate: false
                }
            });
    }

    angular
        .module('app.dashboard')
        .config(['$stateProvider', districtCropYeildByYearRoute]);
})();
