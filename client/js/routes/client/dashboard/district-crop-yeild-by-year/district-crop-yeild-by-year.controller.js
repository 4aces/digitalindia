﻿/**
 * SignIn controller.
 *
 */
(function() {
    'use strict';

    function DistrictCropYeildByYearCtrl(dashboardService, masterService, stateMaster) {

        var vm = this;
        vm.query = {
            state: '',
            district: '',
            crops: []
        };

        vm.StateMaster = stateMaster;
        vm.queryStateSearch = queryStateSearch;
        vm.selectedStateChange = selectedStateChange;
        vm.searchStateChange = searchStateChange;
        //console.log(dashboardService);

        function queryStateSearch(query) {
            var results = query ? vm.StateMaster.filter(createFilterFor(query)) : vm.StateMaster;
            return results;
        }

        function searchStateChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedStateChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));
            vm.DistrictMaster = [];
            vm.cropMaster = [];

            if (item === undefined) {
                console.log(vm.selectedState);
                console.log(vm.searchState);
                vm.query.state = '';
                vm.selectedDistrict = undefined;
                vm.searchDistrict = '';
                vm.selectedCrops = [];
                vm.searchCrop = '';
                vm.production = {};
            } else {
                vm.query.state = item.value;
                getDistricts(item.key);
            }
            //vm.getIndustries();

            //getDashboardData();
        }


        vm.DistrictMaster = [];
        vm.queryDistrictSearch = queryDistrictSearch;
        vm.selectedDistrictChange = selectedDistrictChange;
        vm.searchDistrictChange = searchDistrictChange;

        function queryDistrictSearch(query) {
            var results = query ? vm.DistrictMaster.filter(createFilterFor(query)) : vm.DistrictMaster;
            return results;
        }

        function searchDistrictChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedDistrictChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));

            vm.cropMaster = [];
            if (item === undefined) {
                vm.query.district = '';

                vm.selectedCrops = [];
                vm.searchCrop = '';
                vm.production = {};

            } else {
                vm.query.district = item.value;
                getCrops(item.key);
            }
            //vm.getIndustries();

            // getDashboardData();

        }
        vm.cropMaster = [];
        vm.selectedCrops = [];
        vm.queryCropSearch = queryCropSearch;
        vm.selectedCropChange = selectedCropChange;
        vm.searchCropChange = searchCropChange;
        vm.transformChip = transformChip;
        vm.onCropRemove = getDashboardData;
        vm.onCropAdd = getDashboardData;

        function transformChip(chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

            // Otherwise, create a new one
            //return { name: chip, type: 'new' }
        }



        function queryCropSearch(query) {
            var results = query ? vm.cropMaster.filter(createFilterFor(query)) : vm.cropMaster;
            return results;
        }

        function searchCropChange(text) {
            console.log('Text changed to ' + text);

        }

        function selectedCropChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));

            //console.lo g(JSON.stringify(vm.selectedCrops));
           

        }

        function createFilterFor(query) {
            console.log(query);
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(Data) {
                //console.log(District);
                //console.log(lowercaseQuery);
                return (Data.value.toLowerCase().indexOf(lowercaseQuery) === 0);
            };
        }

        function getDistricts(id) {
            return masterService.getDistrictsByState(id).then(
                function(response) {
                    vm.DistrictMaster = response;
                },
                function(err) {
                    console.log(err);
                });
        }

        function getCrops(id) {
            return masterService.getCropsByDistrict(id).then(
                function(response) {
                    vm.cropMaster = response;
                    //return response;
                },
                function(err) {
                    console.log(err);
                });
        }

        function getDashboardData() {

            if (vm.selectedCrops.length < 1)
                return;

            vm.query.district = vm.selectedDistrict.value;
            vm.query.crops = vm.selectedCrops.map(function(c) {
                return c.value;
            });

            dashboardService.getDistrictCropYeildByYear(vm.query).then(function(response) {
                console.log(response);

                vm.production = response.production;
                //vm.area = response.area;
            }, function(err) {
                console.log(err);
            });

        }



        function boot() {

            vm.selectedState = { key: '579b943f44c0110f9a315b6e', value: 'BIHAR' };

            getDistricts('579b943f44c0110f9a315b6e').then(function() {

                vm.selectedDistrict = { key: '57ac3081865f65f3f19a7d3a', value: 'VAISHALI' }
            });
            getCrops('57ac3081865f65f3f19a7d3a').then(function() {
                vm.selectedCrops = [{ "key": "578b9f62b746958023853057", "value": "Wheat" },
                    { "key": "578b9f62b746958023853050", "value": "Rice" },
                    { "key": "578b9f62b74695802385306f", "value": "Arhar" }
                ];

                getDashboardData();
            });



        }
        boot();


    }
    angular
        .module('app.dashboard')
        .controller('DistrictCropYeildByYearCtrl', ['dashboardService', 'masterService', 'stateMaster', DistrictCropYeildByYearCtrl]);

})();
