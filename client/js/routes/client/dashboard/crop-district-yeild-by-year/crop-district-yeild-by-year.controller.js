﻿/**
 * SignIn controller.
 *
 */
(function() {
    'use strict';

    function CropDistrictYeildByYearCtrl($scope, dashboardService, masterService, stateMaster, cropMaster) {

        var vm = this;
        console.log(dashboardService);

        vm.query = {
            state: '',
            districts: [],
            crop: ''
        };


        vm.cropMaster = cropMaster;
        vm.queryCropSearch = queryCropSearch;
        vm.selectedCropChange = selectedCropChange;
        vm.searchCropChange = searchCropChange;

        function queryCropSearch(query) {
            var results = query ? vm.cropMaster.filter(createFilterFor(query)) : vm.cropMaster;
            return results;
        }

        function searchCropChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedCropChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));
            if (item) {
                vm.query.q = item.value;
            } else {
                vm.query.q = '';
            }
            //  vm.getCrops();
        }


        vm.StateMaster = stateMaster;
        vm.queryStateSearch = queryStateSearch;
        vm.selectedStateChange = selectedStateChange;
        vm.searchStateChange = searchStateChange;
        //console.log(dashboardService);

        function queryStateSearch(query) {
            var results = query ? vm.StateMaster.filter(createFilterFor(query)) : vm.StateMaster;
            return results;
        }

        function searchStateChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedStateChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));
            vm.DistrictMaster = [];
            if (item === undefined) {
                console.log(vm.selectedState);
                console.log(vm.searchState);
                vm.query.state = '';
                vm.selectedDistrict = undefined;
                vm.searchDistrict = '';
                vm.selectedDistricts = [];
                vm.searchDistrict = '';
            } else {
                vm.query.state = item.value;
                getDistricts(item.key);
            }
            //vm.getIndustries();

            //getDashboardData();
        }


        vm.DistrictMaster = [];
        vm.selectedDistricts = [];
        vm.queryDistrictSearch = queryDistrictSearch;
        vm.selectedDistrictChange = selectedDistrictChange;
        vm.searchDistrictChange = searchDistrictChange;
        vm.onDistrictRemove = getDashboardData;
         vm.onDistrictAdd = getDashboardData;

        function queryDistrictSearch(query) {
            var results = query ? vm.DistrictMaster.filter(createFilterFor(query)) : vm.DistrictMaster;
            return results;
        }

        function searchDistrictChange(text) {
            console.log('Text changed to ' + text);

            // getDashboardData();
        }

        function selectedDistrictChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));
            if (item === undefined) {
                vm.query.district = '';

                vm.selectedYear = undefined;
                vm.searchYear = '';

            } else {
                //vm.query.district = item.value;
                // getYears(item.key);
            }
            //vm.getIndustries();

           // getDashboardData();

        }



        function createFilterFor(query) {
            console.log(query);
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(Data) {
                //console.log(District);
                //console.log(lowercaseQuery);
                return (Data.value.toLowerCase().indexOf(lowercaseQuery) === 0);
            };
        }

        vm.transformChip = transformChip;

        function transformChip(chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

            // Otherwise, create a new one
            //return { name: chip, type: 'new' }
        }

        function getDistricts(id) {
            return masterService.getDistrictsByState(id).then(
                function(response) {
                    vm.DistrictMaster = response;
                },
                function(err) {
                    console.log(err);
                });
        }

        function getDashboardData() {

            if (vm.selectedDistricts.length < 1)
                return;
            vm.query.crop = vm.selectedCrop.value;
            vm.query.districts = vm.selectedDistricts.map(function(c) {
                return c.value;
            });

            dashboardService.getCropDistrictYeildByYear(vm.query).then(function(response) {
                console.log(response);

                vm.production = response.production;
                //vm.area = response.area;
            }, function(err) {
                console.log(err);
            });
        }


        function boot() {

            vm.selectedCrop = { key: '578b9f62b74695802385306f', value: 'Arhar' };
            vm.selectedState = { key: '579b943f44c0110f9a315b6e', value: 'BIHAR' };
            getDistricts('579b943f44c0110f9a315b6e').then(function() {

                vm.selectedDistricts = [{ key: '57ac3081865f65f3f19a7d3a', value: 'VAISHALI' },
                    { key: '57ac3081865f65f3f19a7d5a', value: 'SAMASTIPUR' },
                    { key: '57ac3081865f65f3f19a7d5e', value: 'MUZAFFARPUR' }
                ];

                getDashboardData();


            });

        }

        boot();



    }
    angular
        .module('app.dashboard')
        .controller('CropDistrictYeildByYearCtrl', ['$scope', 'dashboardService', 'masterService', 'stateMaster', 'cropMaster', CropDistrictYeildByYearCtrl]);

})();
