﻿/**
 * Signin route.
 *
 */
(function() {
    'use strict';

    /**
     * @ngdoc object
     * @name signinRoute
     * @module app.signin
     * @requires $stateProvider
     * @description
     * Router for the signin page.
     *
     * @ngInject
     */
    function cropDistrictYeildByYearRoute($stateProvider) {
        $stateProvider
            .state('client.cropDistrictYeildByYear', {
                url: '/dashboard/crop-district-yeild-by-year',
                views: {
                    clientContent: {
                        templateUrl: '/js/routes/client/dashboard/crop-district-yeild-by-year/crop-district-yeild-by-year.html',
                        controller: 'CropDistrictYeildByYearCtrl as vm'
                    }
                },
                resolve: {
                    stateMaster: ['masterService', function(masterService) {
                        return masterService.getStates().then(function(response) {
                            return response;
                        }, function(err) {
                            console.log(err)
                            return err;
                        });
                    }],
                    cropMaster: ['masterService', function(masterService) {
                        return masterService.getCrops();
                    }]
                },
                data: {
                    authenticate: false
                }
            });
    }

    angular
        .module('app.dashboard')
        .config(['$stateProvider', cropDistrictYeildByYearRoute]);
})();
