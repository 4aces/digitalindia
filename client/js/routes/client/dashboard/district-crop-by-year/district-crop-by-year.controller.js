﻿/**
 * SignIn controller.
 *
 */
(function() {
    'use strict';

    function DistrictCropByYearCtrl(dashboardService, stateMaster, masterService) {

        var vm = this;
        vm.query = {
            state: '',
            district: '',
            year: ''
        };

        vm.StateMaster = stateMaster;
        vm.queryStateSearch = queryStateSearch;
        vm.selectedStateChange = selectedStateChange;
        vm.searchStateChange = searchStateChange;
        //console.log(dashboardService);

        function queryStateSearch(query) {
            var results = query ? vm.StateMaster.filter(createFilterFor(query)) : vm.StateMaster;
            return results;
        }

        function searchStateChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedStateChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));
            vm.DistrictMaster = [];
            if (item === undefined) {
                console.log(vm.selectedState);
                console.log(vm.searchState);
                vm.query.state = '';
                vm.selectedDistrict = undefined;
                vm.searchDistrict = '';
            } else {
                vm.query.state = item.value;
                getDistricts(item.key);
            }
            //vm.getIndustries();

            getDashboardData();
        }


        vm.DistrictMaster = [];
        vm.queryDistrictSearch = queryDistrictSearch;
        vm.selectedDistrictChange = selectedDistrictChange;
        vm.searchDistrictChange = searchDistrictChange;

        function queryDistrictSearch(query) {
            var results = query ? vm.DistrictMaster.filter(createFilterFor(query)) : vm.DistrictMaster;
            return results;
        }

        function searchDistrictChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedDistrictChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));

            vm.cropMaster = [];
            if (item === undefined) {
                vm.query.district = '';

                vm.selectedYear = undefined;
                vm.searchYear = '';

            } else {
                vm.query.district = item.value;
                // getYears(item.key);
            }
            //vm.getIndustries();

            getDashboardData();

        }

        vm.YearMaster = masterService.getYears();
        vm.queryYearSearch = queryYearSearch;
        vm.selectedYearChange = selectedYearChange;
        vm.searchYearChange = searchYearChange;

        function queryYearSearch(query) {
            var results = query ? vm.YearMaster.filter(createFilterFor(query)) : vm.YearMaster;
            return results;
        }

        function searchYearChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedYearChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));
            if (item === undefined) {
                vm.query.year = '';
            } else {
                vm.query.year = item.key;
            }

            getDashboardData();
            // vm.getIndustries();


        }

        function createFilterFor(query) {
            console.log(query);
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(Data) {
                //console.log(District);
                //console.log(lowercaseQuery);
                return (Data.value.toLowerCase().indexOf(lowercaseQuery) === 0);
            };
        }

        function getDistricts(id) {
            masterService.getDistrictsByState(id).then(
                function(response) {
                    vm.DistrictMaster = response;
                },
                function(err) {
                    console.log(err);
                });
        }

        function getDashboardData() {
            dashboardService.getDistrictCropByYear(vm.query).then(function(response) {
                console.log(response);

                vm.production = response.production;
                vm.area = response.area;
            }, function(err) {
                console.log(err);
            });
        }
        getDashboardData();



    }
    angular
        .module('app.dashboard')
        .controller('DistrictCropByYearCtrl', ['dashboardService', 'stateMaster', 'masterService', DistrictCropByYearCtrl]);

})();
