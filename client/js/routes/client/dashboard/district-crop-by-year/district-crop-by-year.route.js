﻿/**
 * Signin route.
 *
 */
(function() {
    'use strict';

    /**
     * @ngdoc object
     * @name signinRoute
     * @module app.signin
     * @requires $stateProvider
     * @description
     * Router for the signin page.
     *
     * @ngInject
     */
    function districtCropByYearRoute($stateProvider) {
        $stateProvider
            .state('client.districtCropByYear', {
                url: '/dashboard/district-crop-by-year',
                views: {
                    clientContent: {
                        templateUrl: '/js/routes/client/dashboard/district-crop-by-year/district-crop-by-year.html',
                        controller: 'DistrictCropByYearCtrl as vm'
                    }
                },
                resolve: {
                    stateMaster: ['masterService', function(masterService) {
                        return masterService.getStates().then(function(response) {
                            return response;
                        }, function(err) {
                            console.log(err)
                            return err;
                        });
                    }]
                },
                data: {
                    authenticate: false
                }
            });
    }

    angular
        .module('app.dashboard')
        .config(['$stateProvider', districtCropByYearRoute]);
})();
