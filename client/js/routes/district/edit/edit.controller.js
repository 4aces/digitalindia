  (function() {
      'use strict';

      function EditCtrl($mdDialog, $window, district, cropMaster, districtService) {

          console.log(district);
          var vm = this;

          vm.district = district;
          vm.CropMaster = cropMaster;
          vm.queryCropSearch = queryCropSearch;
          vm.selectedCropChange = selectedCropChange;
          vm.searchCropChange = searchCropChange;
          vm.transformChip = transformChip;

          vm.submitDisabled = false;
          vm.showProgress = false;
          vm.isSuccess = false;
          vm.msgShow = false;
          vm.cancel = function() {
              $mdDialog.cancel();
              $window.close();
          };

          vm.selectedCrops = [];

          if (vm.district.CropIds) {

              vm.district.CropIds.forEach(function(id) {

                  for (var i = 0; i < vm.CropMaster.length; i++) {

                      var Crop = vm.CropMaster[i];

                      if (Crop.key === id) {
                          vm.selectedCrops.push(Crop);
                      }
                  }
              });
          }

          vm.edit = function() {

              vm.district.CropIds = [];

              vm.selectedCrops.forEach(function(value) {
                  vm.district.CropIds.push(value.key);
              });

              vm.submitDisabled = true;
              vm.showProgress = true;
              districtService.updateById(vm.district._id,vm.district).then(function(response) {
                  console.log(response);
                  vm.isSuccess = true;
                  vm.showProgress = false;
                  vm.msgShow = true;
                  vm.responseMsg = "Sucessfully edited";
              }, function(err) {
                  vm.submitDisabled = false;
                  vm.msgShow = true;
                  vm.responseMsg = err.join(',');
                  console.log(err);
              });
          };

          function transformChip(chip) {
              // If it is an object, it's already a known chip
              if (angular.isObject(chip)) {
                  return chip;
              }

              // Otherwise, create a new one
              //return { name: chip, type: 'new' }
          }

          function queryCropSearch(query) {
              var results = query ? vm.CropMaster.filter(createFilterFor(query)) : vm.CropMaster;
              return results;
          }

          function searchCropChange(text) {
              console.log('Text changed to ' + text);
          }

          function selectedCropChange(item) {
              console.log('Item changed to ' + JSON.stringify(item));

          }

          /**
           * Create filter function for a query string
           */
          function createFilterFor(query) {
              console.log(query);
              var lowercaseQuery = angular.lowercase(query);
              return function filterFn(Crop) {
                  //console.log(Crop);
                  //console.log(lowercaseQuery);
                  return (Crop.value.toLowerCase().indexOf(lowercaseQuery) === 0);
              };
          }

      }

      angular
          .module('app.district')
          .controller('DistrictEditCtrl', ['$mdDialog', '$window', 'district', 'cropMaster', 'districtService', EditCtrl]);
  })();
