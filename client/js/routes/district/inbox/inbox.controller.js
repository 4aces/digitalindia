/**
 * SignIn controller.
 *
 */
(function() {
    'use strict';

    function InboxCtrl($mdMedia, $mdDialog, districtService, districtMater) {

        //console.log(DistrictMater);
        var vm = this;


        // list of `state` value/display objects
        vm.DistrictMaster = districtMater;
        vm.queryDistrictSearch = queryDistrictSearch;
        vm.selectedDistrictChange = selectedDistrictChange;
        vm.searchDistrictChange = searchDistrictChange;


        vm.query = {
            order: 'Name',
            limit: 10,
            page: 1,
            q: ''
        };

        vm.getDistricts = function() {
            console.log(vm.query);
            vm.promise = districtService.get(vm.query).then(
                function(response) {
                    console.log(response);
                    vm.districts = response;
                    return response;
                },
                function(err) {
                    console.log(err);
                    return err;
                });
        };
        vm.getDistricts();

        vm.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

        vm.openCreate = function(ev) {



            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && vm.customFullscreen;

            $mdDialog.show({
                    controller: 'DistrictCreateCtrl as vm',
                    templateUrl: 'js/routes/district/create/create.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: useFullScreen,
                    resolve: {
                        DistrictMaster: ['masterService', function(masterService) {
                            return masterService.getIndustries();
                        }]
                    }
                })
                .then(function(answer) {
                    vm.getDistricts();
                    //$scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    vm.getDistricts();
                    //$scope.status = 'You cancelled the dialog.';
                });
        };


        vm.openEdit = function(ev, id) {

            console.log('District');
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && vm.customFullscreen;

            $mdDialog.show({
                    controller: 'DistrictEditCtrl as vm',
                    templateUrl: 'js/routes/district/edit/edit.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: useFullScreen,
                    resolve: {
                        district: ['districtService', function(districtService) {
                            return districtService.getById(id)
                        }],
                        cropMaster: ['masterService', function(masterService) {
                            return masterService.getCrops();
                        }]
                    }
                })
                .then(function(answer) {
                    vm.getDistricts();

                    //$scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    vm.getDistricts();

                    //$scope.status = 'You cancelled the dialog.';
                });
        };
        /*$http.get('http://vocab.nic.in/rest.php/states/json').then(function(response) {
            console.log(response);
        });*/

        vm.showCrops = function(ev,item) {
            console.log(item);
        }

        function queryDistrictSearch(query) {
            var results = query ? vm.DistrictMaster.filter(createFilterFor(query)) : vm.DistrictMaster;
            return results;
        }

        function searchDistrictChange(text) {
            console.log('Text changed to ' + text);
        }

        function selectedDistrictChange(item) {
            console.log('Item changed to ' + JSON.stringify(item));
            if (item) {
                vm.query.q = item.value;
            } else {
                vm.query.q = '';
            }
            vm.getDistricts();
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            console.log(query);
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(District) {
                //console.log(District);
                //console.log(lowercaseQuery);
                return (District.value.toLowerCase().indexOf(lowercaseQuery) === 0);
            };
        }

    }
    angular
        .module('app.district')
        .controller('DistrictInboxCtrl', ['$mdMedia', '$mdDialog', 'districtService', 'districtMater', InboxCtrl]);

})();
