﻿/**
 * Signin route.
 *
 */
(function() {
    'use strict';

    /**
     * @ngdoc object
     * @name signinRoute
     * @module app.signin
     * @requires $stateProvider
     * @description
     * Router for the signin page.
     *
     * @ngInject
     */
    function InboxRoute($stateProvider) {
        $stateProvider
            .state('admin.districtinbox', {
                url: '/district/inbox',
                views: {
                    adminContent: {
                        templateUrl: '/js/routes/district/inbox/inbox.html',
                        controller: 'DistrictInboxCtrl as vm'
                    }
                },
                resolve: {
                    districtMater: ['masterService', function(masterService) {
                        return masterService.getDistricts().then(function(response) {
                            return response;
                        }, function(err) {
                            console.log(err)
                            return err;
                        });
                    }]
                },
                data: {
                    authenticate: false
                }
            });
    }

    angular
        .module('app.district')
        .config(['$stateProvider', InboxRoute]);
})();
