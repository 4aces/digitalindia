  (function() {
      'use strict';

      function CreateCtrl($mdDialog, $window, industryMaster, districtService) {

          console.log(industryMaster);
          var vm = this;

          vm.industryMaster = industryMaster;
          vm.queryIndustrySearch = queryIndustrySearch;
          vm.selectedIndustryChange = selectedIndustryChange;
          vm.searchIndustryChange = searchIndustryChange;
          vm.transformChip = transformChip;

          vm.submitDisabled = false;
          vm.showProgress = false;
          vm.isSuccess = false;
          vm.msgShow = false;
          vm.cancel = function() {
              $mdDialog.cancel();
              $window.close();
          };

          vm.selectedIndustrys = [];


          vm.create = function(district) {

            district.IndustryIds=[];

            vm.selectedIndustrys.forEach(function(value){
              district.IndustryIds.push(value.key);
            });

              vm.submitDisabled = true;
              vm.showProgress = true;
              districtService.create(district).then(function(response) {
                  console.log(response);
                  vm.isSuccess = true;
                  vm.showProgress = false;
                  vm.msgShow = true;
                  vm.responseMsg = "Sucessfully added";
              }, function(err) {
                  vm.submitDisabled = false;
                  vm.msgShow = true;
                  vm.responseMsg = err.join(',');
                  console.log(err);
              });
          };

          function transformChip(chip) {
              // If it is an object, it's already a known chip
              if (angular.isObject(chip)) {
                  return chip;
              }

              // Otherwise, create a new one
              //return { name: chip, type: 'new' }
          }

          function queryIndustrySearch(query) {
              var results = query ? vm.industryMaster.filter(createFilterFor(query)) : vm.industryMaster;
              return results;
          }

          function searchIndustryChange(text) {
              console.log('Text changed to ' + text);
          }

          function selectedIndustryChange(item) {
              console.log('Item changed to ' + JSON.stringify(item));

          }

          /**
           * Create filter function for a query string
           */
          function createFilterFor(query) {
              console.log(query);
              var lowercaseQuery = angular.lowercase(query);
              return function filterFn(Industry) {
                  //console.log(Industry);
                  //console.log(lowercaseQuery);
                  return (Industry.value.toLowerCase().indexOf(lowercaseQuery) === 0);
              };
          }

      }

      angular
          .module('app.industry')
          .controller('DistrictCreateCtrl', ['$mdDialog', '$window', 'industryMaster', 'districtService', CreateCtrl]);
  })();
