

# ${projectname}



## Usage

	>mongoimport --db digitalindia --collection districts --type json --file district-crop.json --jsonArray

	>mongoimport --db digitalindia --collection productions --type json --file production_output.json --jsonArray

    >mongoimport -d digitalindia -c districtcrops --type csv --file apy.csv --headerline

        this will import excel data to digitalindia database in mongodb
        after this you can view the data in cmd by using following command

    > mongo
            MongoDB shell version: 3.0.4
            connecting to: test

    > use digitalindia
       	switched to db digitalindia

    > db.districtcrops.find()



## Developing



### Tools

Created with [Nodeclipse](https://github.com/Nodeclipse/nodeclipse-1)
 ([Eclipse Marketplace](http://marketplace.eclipse.org/content/nodeclipse), [site](http://www.nodeclipse.org))   

Nodeclipse is free open-source project that grows with your contributions.
