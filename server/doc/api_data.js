define({ "api": [
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "src/district-crop/districtCrop.dao.js",
    "group": "D__NodeApp_digital_India_server_src_district_crop_districtCrop_dao_js",
    "groupTitle": "D__NodeApp_digital_India_server_src_district_crop_districtCrop_dao_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "src/image/image.controller.js",
    "group": "D__NodeApp_digital_India_server_src_image_image_controller_js",
    "groupTitle": "D__NodeApp_digital_India_server_src_image_image_controller_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "src/image/image.controller.js",
    "group": "D__NodeApp_digital_India_server_src_image_image_controller_js",
    "groupTitle": "D__NodeApp_digital_India_server_src_image_image_controller_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "src/image/image.controller.js",
    "group": "D__NodeApp_digital_India_server_src_image_image_controller_js",
    "groupTitle": "D__NodeApp_digital_India_server_src_image_image_controller_js",
    "name": "Public"
  },
  {
    "type": "get",
    "url": "/district?",
    "title": "",
    "name": "GetUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>Optional page with default 1</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "limit",
            "defaultValue": "25",
            "description": "<p>Optional page limit with default 25</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Crops",
            "description": "<p>District wise crop.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/district-crop/districtCrop.controller.js",
    "groupTitle": "User"
  }
] });
