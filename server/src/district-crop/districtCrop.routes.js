/**
 * Image routes.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license	  The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */
var districtCrop = require('./districtCrop.controller.js');
//var authentication = require('../authentication/authentication.controller.js');

/**
 * Set image routes.
 *
 * @param {Object} app The express application
 */


function setDistrictRoutes(app) {
    app.route('/api/opendata/all')
        //.post(authentication.isAuthenticated, image.create)
        .get(districtCrop.findDistrictCrop);
    //app.route('/images/:id').delete(authentication.isAuthenticated, image.delete);

    app.route('/api/opendata/crops').get(districtCrop.getAllCrops);
    app.route('/api/opendata/district/crop').get(districtCrop.getDistrictWisrCrop);
   app.route('/api/opendata/states').get(districtCrop.getAllStates);

}

module.exports = setDistrictRoutes;
