/**
 * User controller.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */

var logger = require('mm-node-logger')(module);
var link = require('../utils/links-utils.js');
var dao = require('./districtCrop.dao.js');
var Q = require('q');

/**
 * All district Crop .
 *
 * @param {Object} req The request object
 * @param {Object} res The request object
 * @returns {Array} the list of all district Crop 
 * 
 * @api {get} /district? 
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} [page=1]  Optional page with default 1
 * @apiParam {Number} [limit=25]  Optional page limit with default 25
 *
 * @apiSuccess {Object} Crops District wise crop.
 *
 */

function findDistrictCrop(req, res) {

    var page = 1;
    var limit = 25;

    var params = {};
    if (req.query) {
        params = req.query;
    }
    if (req.query.page) {
        page = Number(req.query.page);
    }

    if (req.query.limit) {
        limit = Number(req.query.limit);
    }



    var responseData = {};
    var crops = dao.getDistrictCrop(params);
    var totalCrops = dao.getTotalCropCount(params);

    Q.spread([crops, totalCrops], function(crops, totalCrops) {
        responseData.crops = crops;
        //
        var prev = link.creatLink(req, 'opendata/all', { page: page - 1, limit: limit });
        var next = link.creatLink(req, 'opendata/all', { page: page + 1, limit: limit });

        responseData.paging = {};
        if (page > 1) {
            responseData.paging.previous = prev;
        }
        responseData.paging.next = next;
        responseData.total = totalCrops;

        return res.json(responseData);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });

}

function getAllCrops(req, res) {
var list=[];
    dao.getAllCrops().then(function(response) {

console.log(response[0]);
        response.forEach(function(x){
            var obj = {};
            obj.Name = x._id.Crop;
            obj.Description=x._id.Crop;

            list.push(obj);
        });

        return res.json(list);
    }, function(err) {
        return res.status(500).send(err);
    });
}

function getDistrictWisrCrop(req,res)
{
    dao.getDistrictWisrCrop().then(function(response) {
        return res.json(response);
    }, function(err) {
        return res.status(500).send(err);
    });
}
function getAllStates(req,res)
{
    dao.getAllStates().then(function(response) {
        return res.json(response);
    }, function(err) {
        return res.status(500).send(err);
    });
}

module.exports = {
    findDistrictCrop: findDistrictCrop,
    getAllCrops:getAllCrops,
    getDistrictWisrCrop:getDistrictWisrCrop,
    getAllStates:getAllStates
};
