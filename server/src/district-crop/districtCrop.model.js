/**
 * Image model.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
/**
 * District Crop Schema
 */
var DistrictCropSchema = new mongoose.Schema({
    Year: {
        type: Number
    },
    State: {
        type: String,
        trim: true,
        required: true
    },
    District: {
        type: String,
        trim: true,
        required: true
    },
    Crop: {
        type: String,
        trim: true,
        required: true
    },
    Season: {
        type: String,
        trim: true,
        required: true
    },
    Area: {
        type: Number
    },
    Production: {
        type: Number
    }
});

module.exports = mongoose.model('DistrictCrop', DistrictCropSchema);
