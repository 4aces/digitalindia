/**
 * User DAO.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */

//var logger = require('mm-node-logger')(module);

var DistrictCrop = require('./districtCrop.model.js');


/**
 * All district Crop .
 *
 * @param {Object} req The request object
 * @param {Object} res The request object
 * @returns {Array} the list of all district Crop 
 * 
 */
function getDistrictCrop(params) {
    var page = 1;
    var limit = 25;
    if (params.page) {
        page = Number(params.page);
    }

    if (params.limit) {
        limit = Number(params.limit);
    }

    var filter = {};

    if (params.q) {
        filter.District = new RegExp('^' + params.q, 'i');
        //  filter.State = new RegExp("/^Bih$/i");
    }

    var order = '-Year';

    if (params.order) {
        order = params.order;
    }

    //{'District_lower':'VAISHALI'}

    return DistrictCrop.find(filter, null, {
        skip: (limit * (page - 1)),
        limit: limit,
        sort: order
    }).exec();

}

function getTotalCropCount(params) {
    var filter = {};

    if (params.q) {
        filter.District = new RegExp('^' + params.q, 'i');
        //  filter.State = new RegExp("/^Bih$/i");
    }
    return DistrictCrop.count(filter).exec();
}

/*function getAllCrops() {
    return DistrictCrop.aggregate([{
            $group: {
                _id: { 'Crop': '$Crop' },
                production: { $sum: '$Production' },
                area: { $sum: '$Area' }
            }
        },
        { '$sort': { 'production': -1 } },
    ]).exec();
}*/

function getAllCrops() {
    return DistrictCrop.aggregate([{
        $group: {
            _id: { 'Crop': '$Crop' },
            //production: { $sum: '$Production' },
            //area: { $sum: '$Area' }
        }
    }]).exec();
}

function getDistrictWisrCrop() {
    return DistrictCrop.aggregate(
        [
            { $group: { _id: { 'District': '$District', 'State': '$State' }, Crop: { $addToSet: "$Crop" } } },
            { $sort: { "_id.State": -1 } },
            { $project: { '_id': 0, State: '$_id.State', 'District': '$_id.District', 'CropIds': '$Crop' } }
        ]).exec();

}

function getAllStates() {
    return DistrictCrop.aggregate([
        { $group: { _id: { 'State': '$State' } } },
        { $project: { '_id': 0, State: '$_id.State' } }
    ]).exec();
}


module.exports = {
    getDistrictCrop: getDistrictCrop,
    getTotalCropCount: getTotalCropCount,
    getAllCrops: getAllCrops,
    getDistrictWisrCrop: getDistrictWisrCrop,
    getAllStates:getAllStates
};
