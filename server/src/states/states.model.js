'use strict';

/**
 * Module dependencies.
 */

var mongoose = require('mongoose');

/**
 * State Schema

 */
var StateSchema = new mongoose.Schema({
    Name: {
		type: String,
		required : true
	}
	 
});

module.exports = mongoose.model('State', StateSchema);



