/**
 * User controller.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */

var logger = require('mm-node-logger')(module);
var Q = require('q');
var link = require('../utils/links-utils.js');
var dao = require('./crop.dao.js');


/**
 * All district Crop .
 *
 * @param {Object} req The request object
 * @param {Object} res The request object
 * @returns {Array} the list of all district Crop 
 * 
 * @api {get} /district? 
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} [page=1]  Optional page with default 1
 * @apiParam {Number} [limit=25]  Optional page limit with default 25
 *
 * @apiSuccess {Object} Crops District wise crop.
 *
 */

function createCrop(req, res) {

    var crop = req.body;

    dao.createCrop(crop).then(function(response) {
        return res.json(response);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });
}

function getCrop(req, res) {
    var page = 1;
    var limit = 25;

    var params = {};
    if (req.query) {
        params = req.query;
    }
    if (req.query.page) {
        page = Number(req.query.page);
    }

    if (req.query.limit) {
        limit = Number(req.query.limit);
    }


    var responseData = {};
    var crops = dao.getCrop(params);
    var totalCrops = dao.getTotalCropCount(params);

    Q.spread([crops, totalCrops], function(crops, totalCrops) {
        responseData.crops = crops;
        //
        var prev = link.creatLink(req, 'crops', { page: page - 1, limit: limit });
        var next = link.creatLink(req, 'crops', { page: page + 1, limit: limit });

        responseData.paging = {};
        if (page > 1) {
            responseData.paging.previous = prev;
        }
        if (page * limit < totalCrops) {
            responseData.paging.next = next;
        }
        responseData.total = totalCrops;

        return res.json(responseData);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });
}


function getCropById(req, res) {

    var id = req.params.id;

    dao.getCropById(id).then(function(response) {
        return res.json(response);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });
}

function updateCropById(req, res) {
    var id = req.params.id;
    var obj = req.body;
    dao.updateCropById(id, obj).then(function(response) {
        return res.json(response);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });
}

module.exports = {
    createCrop: createCrop,
    getCrop: getCrop,
    getCropById: getCropById,
    updateCropById: updateCropById
};
