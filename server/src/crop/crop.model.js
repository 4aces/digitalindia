/**
 * Image model.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
/**
 *  Crop Schema
 */
var CropSchema = new mongoose.Schema({
    
    Name: {
        type: String,
        trim: true,
        required: true
    },
    Description: {
        type: String,
        trim: true,
        required: true
    },
    Photo: {
        type: String
    },
    Weather: {
        type: String,
        trim: true
    },
    Soil: {
        type: String,
        trim: true
       
    }
});

CropSchema.virtual('key')
.get(function () {
  return this._id;
});

CropSchema.virtual('value')
.get(function () {
  return this.Name;
});


module.exports = mongoose.model('Crop', CropSchema);
