/**
 * Image routes.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license	  The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */
var crop = require('./crop.controller.js');
//var authentication = require('../authentication/authentication.controller.js');

/**
 * Set image routes.
 *
 * @param {Object} app The express application
 */


function setCropRoutes(app) {
    app.route('/api/crops')
    .post(crop.createCrop)
    .get(crop.getCrop);
    
    app.route('/api/crops/:id')
    .get(crop.getCropById)
    .put(crop.updateCropById);
}


module.exports = setCropRoutes;
