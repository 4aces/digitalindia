/**
 * User DAO.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */

//var logger = require('mm-node-logger')(module);

var Crop = require('./crop.model.js');


/**
 * All district Crop .
 *
 * @param {Object} req The request object
 * @param {Object} res The request object
 * @returns {Array} the list of all district Crop 
 * 
 */


function createCrop(crop) {
    return Crop.create(crop);
}

function getCrop(params) {
    var page = 1;
    var limit = 25;
    if (params.page) {
        page = Number(params.page);
    }

    if (params.limit) {
        limit = Number(params.limit);
    }

    var filter = {};
    /**/
    if (params.q) {
        filter.Name = new RegExp('^' + params.q, 'i');
        //  filter.State = new RegExp("/^Bih$/i");
    }
    return Crop.find(filter, { '__v': 0 }, {
        skip: (limit * (page - 1)),
        limit: limit,
        sort:'Name'
    }).exec();
}

function getTotalCropCount(params) {
    var filter = {};

    if (params.q) {
        filter.Name = new RegExp('^' + params.q, 'i');
        //  filter.State = new RegExp("/^Bih$/i");
    }
    return Crop.count(filter).exec();
}

function getCropById(id) {
    return Crop.findById(id).exec();
}

function updateCropById(id, obj) {
    return Crop.update({ _id: id }, {
        $set: obj
    }).exec();
}


module.exports = {
    createCrop: createCrop,
    getCrop: getCrop,
    getCropById: getCropById,
    getTotalCropCount: getTotalCropCount,
    updateCropById:updateCropById
};
