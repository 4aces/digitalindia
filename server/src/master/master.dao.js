/**
 * User DAO.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */

//var logger = require('mm-node-logger')(module);

var Crop = require('../crop/crop.model.js');
var Industry = require('../industry-crop/industryCrop.model.js');
var State = require('../states/states.model');
var District = require('../district/district.model');
var Q = require('q');
/**
 * All district Crop .
 *
 * @param {Object} req The request object
 * @param {Object} res The request object
 * @returns {Array} the list of all district Crop 
 * 
 */


function getCrops() {

    return Crop.aggregate([{
        '$project': {
            '_id': 0,
            'key': '$_id',
            'value': '$Name'

        }
    }]).exec();
}

function getIndustries() {

    return Industry.aggregate([{
        '$project': {
            '_id': 0,
            'key': '$_id',
            'value': '$Name'

        }
    }]).exec();
}

function getStates() {
    return State.aggregate([{
        '$project': {
            '_id': 0,
            'key': '$_id',
            'value': '$Name'

        }
    }]).exec();
    //return State.find().exec();
}

function getDistricts() {
    return District.aggregate([{
        '$project': {
            '_id': 0,
            'key': '$_id',
            'value': '$Name'

        }
    }]).exec();

}

function getDistrictsByState(id) {
    return District.aggregate([
        { $match: { StateId: id } },
        { '$project': { '_id': 0, 'key': '$_id', 'value': '$Name' } }
    ]).exec();

}

function getCropsByDistrict(id) {


    var result = District.findOne({ _id: id }).populate('CropIds', { '__v': 0 }).exec();

    //return result;

    return result.then(function(response) {

        var list = response.CropIds.map(function(x) {
            var obj = {};
            obj.key = x._id;
            obj.value = x.Name;

            return obj;
        })

        console.log(list);

        // return Q.resolve(list);
        return list;

    }, function(err) {

    });

}

module.exports = {
    getCrops: getCrops,
    getIndustries: getIndustries,
    getStates: getStates,
    getDistricts: getDistricts,
    getDistrictsByState: getDistrictsByState,
    getCropsByDistrict: getCropsByDistrict
};
