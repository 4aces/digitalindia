/**
 * User controller.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */

var logger = require('mm-node-logger')(module);
var dao = require('./master.dao.js');


/**
 * All district Crop .
 *
 * @param {Object} req The request object
 * @param {Object} res The request object
 * @returns {Array} the list of all district Crop 
 * 
 * @api {get} /district? 
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} [page=1]  Optional page with default 1
 * @apiParam {Number} [limit=25]  Optional page limit with default 25
 *
 * @apiSuccess {Object} Crops District wise crop.
 *
 */

function getCrops(req, res) {
    dao.getCrops().then(function(response) {
        return res.json(response);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });
}

function getIndustries(req, res) {
    dao.getIndustries().then(function(response) {
        return res.json(response);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });
}

function getStates(req, res) {
    dao.getStates().then(function(response) {
        return res.json(response);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });
}

function getDistricts(req, res) {
    dao.getDistricts().then(function(response) {
        return res.json(response);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });
}

function getDistrictsByState(req, res) {
    dao.getDistrictsByState(req.params.id).then(function(response) {
        return res.json(response);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });
}

function getCropsByDistrict(req, res) {
    dao.getCropsByDistrict(req.params.id).then(function(response) {
        return res.json(response);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });
}
module.exports = {
    getCrops: getCrops,
    getIndustries: getIndustries,
    getStates: getStates,
    getDistricts: getDistricts,
    getDistrictsByState: getDistrictsByState,
    getCropsByDistrict: getCropsByDistrict
};
