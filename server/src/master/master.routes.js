/**
 * Image routes.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license	  The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */
var master = require('./master.controller.js');
//var authentication = require('../authentication/authentication.controller.js');

/**
 * Set image routes.
 *
 * @param {Object} app The express application
 */


function setMasterRoutes(app) {
    app.route('/api/master/crops')
        .get(master.getCrops);
    app.route('/api/master/industries')
        .get(master.getIndustries);
    app.route('/api/master/states')
        .get(master.getStates);
    app.route('/api/master/districts')
        .get(master.getDistricts);
    app.route('/api/master/state/:id/districts').get(master.getDistrictsByState);
    app.route('/api/master/district/:id/crops').get(master.getCropsByDistrict);
}


module.exports = setMasterRoutes;
