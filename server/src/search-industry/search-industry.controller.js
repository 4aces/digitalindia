(function() {
    'use strict';

    var logger = require('mm-node-logger')(module);
    var dao = require('./search-industry.dao.js');
    var industryCtrl = require('../industry-crop/industryCrop.controller.js');
    var Q = require('q');

    function getIndustries(req, res) {

        var page = 1;
        var limit = 25;
        if (req.query.page) {
            page = Number(req.query.page);
        }

        if (req.query.limit) {
            limit = Number(req.query.limit);
        }

        var cropids = [];
        var district = '';
        var state = '';

        if (req.query) {
            if (req.query.crop)
                cropids.push(req.query.crop);

            if (req.query.district)
                district = req.query.district;


            if (req.query.state)
                state = req.query.state;

        }

        if (state == '')
            return industryCtrl.getIndustries(req, res);


        var r = dao.getIndustries(state, district, cropids, page, limit);
        r.then(function(response) {
                //var responseData = {};
                //responseData.industries = response;
                return res.json(response);
            },
            function(error) {
                return res.status(500).send(error);
            });

        // console.log('*******');
        //console.log(r);
    }





    module.exports = {
        getIndustries: getIndustries
    };

})();
