'use strict';

var DistrictCrop = require('../district-crop/districtCrop.model.js');
var IndustryCrop = require('../industry-crop/industryCrop.model.js');
var Crop = require('../crop/crop.model.js');
var District = require('../district/district.model.js');
var ObjectId = require('mongoose').Types.ObjectId;
var Q = require('q');

function union_arrays(x, y) {
    var obj = {};
    for (var i = x.length - 1; i >= 0; --i)
        obj[x[i]] = x[i];
    for (var i = y.length - 1; i >= 0; --i)
        obj[y[i]] = y[i];
    var res = []
    for (var k in obj) {
        if (obj.hasOwnProperty(k)) // <-- optional
            res.push(obj[k]);
    }
    return res;
}

function getDistrictId(state) {
    /*console.log('getDistrictId');
    console.log(state);
    var q = { "StateId": new ObjectId("579b943f44c0110f9a315b6e") };
    console.log(q);
    var s = District.find({StateId:"579b943f44c0110f9a315b6e"}).exec();
    console.log(s);

    return s;*/

    return District.aggregate([
        { $match: { StateId: state } },
        { '$project': { '_id': 0, 'CropIds': '$CropIds' } }
    ]).exec();
}

function getAllCropIds(state, district) {
    console.log(district);


    // console.log('District');
    //console.log(district);
    if (district == '') {
        //return Crop.find({}, { _id: 1 }).exec();

        console.log('getDistrictId start');
        //console.log(state);
        return getDistrictId(state).then(function(response) {
            //console.log(see);
            //console.log('getDistrictId response');
            console.log(response[0]);

            var result = [];
            for (var i = 0; i < response.length; i++) {
                result = union_arrays(result, response[i].CropIds);
                //console.log(result);
            }
            console.log(result);
            return Q.resolve(result);
        }, function(err) {
            console.log(err);

            return err;
        });



        /*return District.aggregate([
    { $match: { StateId: id } },
    { '$project': { '_id': 0, 'key': '$_id', 'value': '$Name' } }
]).exec();
*/
    } else {
        return District.findOne({ _id: district }, { CropIds: 1 }).exec().then(function(response) {
            return Q.resolve(response.CropIds);
        });
    }
}

function getIndustriesFinal(crops, pagination) {



    var industries = IndustryCrop.find({ CropIds: { $in: crops } }, null, pagination).populate('CropIds', { '_id': 0, '__v': 0 }).exec();
    var totalIndustry = getIndustriesTotal(crops);



    console.log(industries);
    console.log(totalIndustry);

    var deferred = Q.defer();

    Q.spread([industries, totalIndustry], function(industries, totalIndustry) {
        var responseData = {};
        responseData.industries = industries;
        responseData.total = totalIndustry;

        deferred.resolve(responseData);
    }, function(err) {
        logger.error(err.message);
        deferred.reject(err);
    });

    return deferred.promise;


}


function getIndustriesTotal(cropIds) {
    return IndustryCrop.count({ CropIds: { $in: cropIds } }).exec();

}

function getIndustries(state, district, crops, page, limit) {

    var pagination = {
        skip: (limit * (page - 1)),
        limit: limit
    };
    console.log(state);
    if (crops.length == 0) {
        //console.log('crops');

        return getAllCropIds(state, district).then(function(cropIds) {
            //console.log(cropIds);
            return Q.resolve(cropIds);
        }).then(function(cropIds) {

            return getIndustriesFinal(cropIds, pagination);
            //console.log("CropIds");
            //console.log(cropIds);
            // return IndustryCrop.find({ CropIds: { $in: cropIds } }, null, pagination).populate('CropIds', { '_id': 0, '__v': 0 }).exec();
            //console.log(r);
            //return r;
        });
    } else {
        return getIndustriesFinal(crops, pagination);
    }
}


module.exports = {
    getIndustries: getIndustries
};
