
'use strict';

/**
 * Module dependencies.
 */
var searchIndustry = require('./search-industry.controller.js');

function setSearchIndustryRoutes(app) {
    app.route('/api/search-industries')
   .get(searchIndustry.getIndustries);

   
}

module.exports = setSearchIndustryRoutes;
