(function() {
    'use strict';

    function Pie(data, options) {
        this.type = "PieChart";
        this.data = data;
        this.options = options;
    }

    function Line(data, options) {
        this.type = "LineChart";
        this.data = data;
        this.options = options;
    }
	function Column(data, options) {
        this.type = "ColumnChart";
        this.data = data;
        this.options = options;
    }

    module.exports.Pie = Pie;
    module.exports.Line = Line;
    module.exports.Column = Column;
})();
