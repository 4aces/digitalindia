(function() {
    'use strict';

    function ColInfo(id, lable = id, type = "number") {
        this.id = id;
        this.label = lable;
        this.type = type;
    }

    function DataPoint(value, format = value) {
        this.v = value;
        this.f = format.toString();
    }

    function DataPointSet() {
        this.c = [];
    }

    DataPointSet.prototype.addDataPoint = function(dp) {
        if (dp instanceof Object && dp instanceof DataPoint) {
            this.c.push(dp);
        } else {
            throw new Error("parameter is not instanceof DataPoint");
        }
    };

    function GraphData() {

        this.cols = [];
        this.rows = [];
    }
    GraphData.prototype.addColumn = function(col) {

        if (col instanceof ColInfo)
            this.cols.push(col);
        else
            throw new Error("column is not instanceof ColInfo");

    };

    GraphData.prototype.addRow = function(row) {
        if (row instanceof DataPointSet)
            this.rows.push(row);
        else
            throw new Error("row is not instanceof DataPointSet");
    }

    function Options(title) {
        this.title = title;
    }
    Options.prototype.setHAxisTitle = function(title) {
        this.hAxis = {
            title: title

        }
    }

    Options.prototype.setVAxisTitle = function(title) {
        this.vAxis = {
            title: title
        }
    }

    module.exports.ColInfo = ColInfo;
    module.exports.DataPoint = DataPoint;
    module.exports.DataPointSet = DataPointSet;
    module.exports.GraphData = GraphData;
    module.exports.Options = Options;

})();
