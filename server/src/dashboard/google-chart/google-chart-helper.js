(function() {
    'use strict';

    var google_chart = require('./google-chart')

    function createGraphData(data) {


        //console.log(data);
        var graphData = new google_chart.GraphData();
        //console.log(data);

        for (var prop in data[0]) {

            //console.log(prop);
            //console.log();

            var col = new google_chart.ColInfo(prop, prop, typeof data[0][prop]);
            //console.log(col);
            graphData.addColumn(col);
        }
        //console.log(graphData)

        for (var d = 0; d < data.length; d++) {

            var dataPointSet = new google_chart.DataPointSet();
            for (var prop in data[d]) {
                var dataPoint = new google_chart.DataPoint(data[d][prop]);
                dataPointSet.addDataPoint(dataPoint);
            }

            graphData.addRow(dataPointSet);
        }
        //console.log(graphData)

        return graphData;
    }


    function setOptions(title) {
         return new google_chart.Options(title);
    }

    module.exports.createGraphData = createGraphData;
    module.exports.setOptions=setOptions;
})();
