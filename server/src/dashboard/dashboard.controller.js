(function() {
    'use strict';

    var logger = require('mm-node-logger')(module);
    var dao = require('./dashboard.dao.js');
    var Q = require('q');
    var google_chart_helper = require('./google-chart/google-chart-helper');
    var google_chart = require('./google-chart/charts');

    function GetDistrictCropRatioByYear(req, res) {

        var district = ['VAISHALI'];
        var year = 2007;

        console.log(district);
        var districtProduction = dao.GetDistrictCropByYearProduction(district, year);
        var nationalProduction = dao.GetCropProductionByYear(year);
        var districtArea = dao.GetDistrictCropByYearArea(district, year);
        var nationalArea = dao.GetCropAreaByYear(year);

        //console.log(districtProduction);
        //console.log(nationalProduction);
        //console.log(districtArea);
        //console.log(nationalArea);

        Q.spread([districtProduction, nationalProduction, districtArea, nationalArea], function(districtProduction, nationalProduction, districtArea, nationalArea) {
            var responseData = {};
            console.log(districtProduction);
            console.log(nationalProduction);
            console.log(districtArea);
            console.log(nationalArea);
            var i;
            var j;
            var data1 = [];
            var data2 = [];

            for (i = 0; i < districtProduction.length; i++) {
                var p = districtProduction[i];
                var cr = p.Crop;
                var pr = p.Production;

                for (j = 0; j < nationalProduction.length; j++) {
                    if (nationalProduction[j].Crop == cr) {
                        var cp = {};
                        cp.Crop = cr;
                        cp.Production = (pr) / (nationalProduction[j].Production);
                        data1.push(cp);
                    }
                }
            }

            console.log(data1);

            for (i = 0; i < districtArea.length; i++) {
                var a = districtArea[i];
                var cr = a.Crop;
                var ar = a.Area;

                for (j = 0; j < nationalArea.length; j++) {
                    if (nationalArea[j].Crop == cr) {
                        var cp = {};
                        cp.Crop = cr;
                        cp.Area = (pr) / (nationalArea[j].Area);
                        data2.push(cp);
                    }
                }
            }

            console.log(data2);
            responseData.production = data1;
            responseData.area = data2;
            //var production_chart_data = google_chart_helper.createGraphData(production);
            //var production_chart_options = google_chart_helper.setOptions("Pie");
            //responseData.production = new google_chart.Pie(production_chart_data, production_chart_options);

            //var area_chart_data = google_chart_helper.createGraphData(area);
            //var area_chart_options = google_chart_helper.setOptions("Pie");
            //responseData.area = new google_chart.Pie(area_chart_data, area_chart_options);

            return res.json(responseData);
        }, function(err) {
            logger.error(err.message);
            return res.status(500).send(err);
        });
    }

    function GetDistrictCropByYear(req, res) {

        var year = 0;
        var district = '';
        var state = '';

        if (req.query) {
            if (req.query.district)
                district = req.query.district;

            if (req.query.year)
                year = parseInt(req.query.year);


            if (req.query.state)
                state = req.query.state;

        }
        //var 
        //district = 'VAISHALI';
        //var year = 2000;

        console.log(district);
        var production = dao.GetDistrictCropByYearProduction(state, district, year);
        var area = dao.GetDistrictCropByYearArea(state, district, year);

        Q.spread([production, area], function(production, area) {
            var responseData = {};
            //    responseData.production = production;
            var production_chart_data = google_chart_helper.createGraphData(production);
            var production_chart_options = google_chart_helper.setOptions("District Crop By Year - Production");
            responseData.production = new google_chart.Pie(production_chart_data, production_chart_options);

            var area_chart_data = google_chart_helper.createGraphData(area);
            var area_chart_options = google_chart_helper.setOptions("District Crop By Year - Area");
            responseData.area = new google_chart.Pie(area_chart_data, area_chart_options);

            return res.json(responseData);
        }, function(err) {
            logger.error(err.message);
            return res.status(500).send(err);
        });
    }

    function GetDistrictCropYeildByYear(req, res) {

        var crops = [];

        if (typeof req.query.crops != "string")
            crops = req.query.crops;
        else
            crops.push(req.query.crops);

        var district = req.query.district;

        var start_year = 1990;
        var end_year = 2010;

        console.log(district);
        console.log(crops);
        var i;
        var yeilds = [];

        for (i = 0; i < crops.length; i++) {
            console.log(crops[i]);
            var yeild = dao.GetDistrictYeildByCrop(district, crops[i]);
            console.log(yeild);
            yeilds.push(yeild);

        }
        //var yeild = dao.GetDistrictYeildByCrop(district, crop);
        //console.log(yeilds);

        //var production = dao.GetDistrictCropByYearProduction(district, year);
        //var area = dao.GetDistrictCropByYearArea(district, year);

        Q.allSettled(yeilds).then(function(yeilds) {
            var data;
            var iter = 0;
            console.log(iter);
            yeilds.forEach(function(yeild) {
                if (yeild.state === "fulfilled") {
                    var value = yeild.value;
                    if (iter == 0) {
                        data = value;
                    } else {
                        var j;
                        for (j = 0; j < value.length; j++) {
                            console.log(JSON.stringify(value[j]));
                            var keys = Object.keys(value[j]);
                            var k;
                            for (k = 0; k < data.length; k++) {
                                var keys2 = Object.keys(data[k]);
                                if (data[k][keys2[0]] == value[j][keys[0]]) {
                                    data[k][keys[1]] = value[j][keys[1]];
                                }

                            }

                        }
                    }
                    iter++;

                } else {
                    var reason = yeild.reason;
                }
                console.log(JSON.stringify(data));
            });

            /*yeild =  yeild.map(function(x){
                x.Year = x.Year.toString(); 

                return x;

            });*/
            var responseData = {};
            //console.log(yeilds);
            //responseData.yeild = data;
            var production_chart_data = google_chart_helper.createGraphData(data);
            var production_chart_options = google_chart_helper.setOptions("District Crop Yield By Year");
            production_chart_options.setHAxisTitle('Year');
            production_chart_options.setVAxisTitle('Yield - ton/hec');
            //responseData.production = new google_chart.Line(production_chart_data, production_chart_options);
            responseData.production = new google_chart.Column(production_chart_data, production_chart_options);

            //var area_chart_data = google_chart_helper.createGraphData(area);
            //var area_chart_options = google_chart_helper.setOptions("Pie");
            //responseData.area = new google_chart.Pie(area_chart_data, area_chart_options);

            return res.json(responseData);
        }, function(err) {
            logger.error(err.message);
            return res.status(500).send(err);
        });
    }

    function GetCropDistrictYeildByYear(req, res) {
        //var district = ['PALAMU', 'VAISHALI', 'SAMASTIPUR'];
        //var crop = 'Arhar';


        var district = [];

        if (typeof req.query.districts != "string")
            district = req.query.districts;
        else
            district.push(req.query.districts);

        var crop = req.query.crop;
        var start_year = 1990;
        var end_year = 2010;

        console.log(district);
        console.log(crop);
        var i;
        var yeilds = [];

        for (i = 0; i < district.length; i++) {
            console.log(district[i]);
            var yeild = dao.GetCropYeildByDistrict(district[i], crop);
            console.log(yeild);
            yeilds.push(yeild);

        }
        //var yeild = dao.GetDistrictYeildByCrop(district, crop);
        //console.log(yeilds);

        //var production = dao.GetDistrictCropByYearProduction(district, year);
        //var area = dao.GetDistrictCropByYearArea(district, year);

        Q.allSettled(yeilds).then(function(yeilds) {
            var data;
            var iter = 0;
            console.log(iter);
            yeilds.forEach(function(yeild) {
                if (yeild.state === "fulfilled") {
                    var value = yeild.value;
                    if (iter == 0) {
                        data = value;
                    } else {
                        var j;
                        for (j = 0; j < value.length; j++) {
                            console.log(JSON.stringify(value[j]));
                            var keys = Object.keys(value[j]);
                            var k;
                            for (k = 0; k < data.length; k++) {
                                var keys2 = Object.keys(data[k]);
                                if (data[k][keys2[0]] == value[j][keys[0]]) {
                                    data[k][keys[1]] = value[j][keys[1]];
                                }

                            }

                        }
                    }
                    iter++;

                } else {
                    var reason = yeild.reason;
                }
                console.log(JSON.stringify(data));
            });

            /*yeild =  yeild.map(function(x){
                x.Year = x.Year.toString(); 

                return x;

            });*/
            var responseData = {};
            //console.log(yeilds);
            //responseData.yeild = data;
            var production_chart_data = google_chart_helper.createGraphData(data);
            var production_chart_options = google_chart_helper.setOptions("Crop District Yeild By Year");
            production_chart_options.setHAxisTitle('Year');
            production_chart_options.setVAxisTitle('Yield - ton/hec');
            responseData.production = new google_chart.Line(production_chart_data, production_chart_options);

            //var area_chart_data = google_chart_helper.createGraphData(area);
            //var area_chart_options = google_chart_helper.setOptions("Pie");
            //responseData.area = new google_chart.Pie(area_chart_data, area_chart_options);

            return res.json(responseData);
        }, function(err) {
            logger.error(err.message);
            return res.status(500).send(err);
        });
    }



    module.exports = {
        GetDistrictCropByYear: GetDistrictCropByYear,
        GetDistrictCropYeildByYear: GetDistrictCropYeildByYear,
        GetCropDistrictYeildByYear: GetCropDistrictYeildByYear,
        GetDistrictCropRatioByYear: GetDistrictCropRatioByYear
    };

})();
