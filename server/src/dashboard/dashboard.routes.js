
'use strict';

/**
 * Module dependencies.
 */
var dashboard = require('./dashboard.controller.js');

function setDashboardRoutes(app) {
    app.route('/api/dashboard/district-crop-by-year')
   .get(dashboard.GetDistrictCropByYear);

   app.route('/api/dashboard/district-crop-yeild-by-year')
   	.get(dashboard.GetDistrictCropYeildByYear);

   	app.route('/api/dashboard/crop-district-yeild-by-year')
   	.get(dashboard.GetCropDistrictYeildByYear);


   	app.route('/api/dashboard/crop-districtNation-ratio-by-year')
   	.get(dashboard.GetDistrictCropRatioByYear);

   	

}

module.exports = setDashboardRoutes;
