'use strict';

var DistrictCrop = require('../district-crop/districtCrop.model.js');

function GetCropProductionByYear(year) {
    return DistrictCrop.aggregate(
        [{ $match: { Year: year } },
            { $group: { _id: { 'Crop': '$Crop' }, production: { $sum: '$Production' } } },
            { $sort: { "_id.production": -1 } },
            { $project: { '_id': 0, Crop: '$_id.Crop', 'Production': '$production' } }
        ]).exec();

}

function GetCropAreaByYear(year) {
    return DistrictCrop.aggregate(
        [{ $match: { Year: year } },
            { $group: { _id: { 'Crop': '$Crop' }, area: { $sum: '$Area' } } },
            { $sort: { "_id.area": -1 } },
            { $project: { '_id': 0, Crop: '$_id.Crop', 'Area': '$area' } }
        ]).exec();

}

function GetDistrictCropByYearProduction(state, district, year) {

    var filter = {};

    if (district != '')
        filter.District = district //{ $in: districts };
    if (year)
        filter.Year = year;

    if (state != '')
        filter.State = state;

    filter.Production = { $gt: 0 };

    console.log(filter);

    return DistrictCrop.aggregate(
        [{ $match: filter },
            { $group: { _id: { 'Crop': '$Crop' }, production: { $sum: '$Production' } } },
            { $sort: { "_id.production": -1 } },
            { $project: { '_id': 0, Crop: '$_id.Crop', 'Production': '$production' } }
        ]).exec();
}

function GetDistrictCropByYearArea(state, district, year) {
    var filter = {};

    if (district != '')
        filter.District = district; //{ $in: districts };
    if (year)
        filter.Year = year;

    filter.Production = { $gt: 0 };
    filter.Area = { $gt: 0 };

    console.log(filter);

    return DistrictCrop.aggregate(
        [{ $match: filter },
            { $group: { _id: { 'Crop': '$Crop' }, area: { $sum: '$Area' } } },
            { $sort: { "_id.area": -1 } },
            { $project: { '_id': 0, Crop: '$_id.Crop', 'Area': '$area' } }
        ]).exec();
}

function GetDistrictYeildByCrop(district, crop) {

    var filter = {};

    if (district != '')
        filter.District = district; //{ $in: districts };
    if (crop != '')
        filter.Crop = crop;

    filter.Production = { $gt: 0 };
    filter.Area = { $gt: 0 };

    console.log(filter);

    return DistrictCrop.aggregate(
        [{ $match: filter },
            { $group: { _id: { 'Year': '$Year' }, production: { $sum: '$Production' }, area: { $sum: '$Area' } } },
            { $sort: { "_id.Year": 1 } },
            { $project: { '_id': 0, Year: '$_id.Year', [crop]: { $divide: ["$production", "$area"] } } }
        ]).exec();
}

function GetCropYeildByDistrict(district, crop) {

var filter = {};

    if (district != '')
        filter.District = district; //{ $in: districts };
    if (crop != '')
        filter.Crop = crop;

    filter.Production = { $gt: 0 };
    filter.Area = { $gt: 0 };

    console.log(filter);

    return DistrictCrop.aggregate(
        [{ $match: { District: district, Crop: crop } },
            { $group: { _id: { 'Year': '$Year' }, production: { $sum: '$Production' }, area: { $sum: '$Area' } } },
            { $sort: { "_id.Year": 1 } },
            { $project: { '_id': 0, Year: '$_id.Year', [district]: { $divide: ["$production", "$area"] } } }
        ]).exec();
}

module.exports = {
    GetDistrictCropByYearProduction: GetDistrictCropByYearProduction,
    GetDistrictCropByYearArea: GetDistrictCropByYearArea,
    GetDistrictYeildByCrop: GetDistrictYeildByCrop,
    GetCropYeildByDistrict: GetCropYeildByDistrict,
    GetCropProductionByYear: GetCropProductionByYear,
    GetCropAreaByYear: GetCropAreaByYear


};
