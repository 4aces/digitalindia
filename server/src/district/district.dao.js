/**
 * User DAO.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */

var logger = require('mm-node-logger')(module);

var District = require('./district.model.js');





/**
 * All district Crop .
 *
 * @param {Object} req The request object
 * @param {Object} res The request object
 * @returns {Array} the list of all district Crop 
 * 
 */

function getDistricts(params) {
    var page = 1;
    var limit = 25;
    if (params.page) {
        page = Number(params.page);
    }

    if (params.limit) {
        limit = Number(params.limit);
    }

    var filter = {};
    /**/
    if (params.q) {
        filter.Name = new RegExp('^' + params.q, 'i');
        //  filter.State = new RegExp("/^Bih$/i");
    }

    var order = 'Name';

    if (params.order) {
        order = params.order;
    }
    return District.find(filter, { '__v': 0 }, {
        skip: (limit * (page - 1)),
        limit: limit,
        sort: order
    }).populate('StateId', 'Name').populate('CropIds').
    exec();
    //.deepPopulate('IndustryIds.CropIds').exec();
    //, { '_id': 0, '__v': 0 }).exec();


}

function createNew(districtIndustry) {

    return District.create(districtIndustry);

}

function updateDistrict(id, obj) {
    return District.update({ _id: id }, {
        $set: obj
    }).exec();
}

function searchById(id) {
    return District.findById({ _id: id }).exec();

}

function removeById(id) {
    return District.remove({ _id: id }).exec();

}

function getTotalDistrictCount(params) {
    var filter = {};

    if (params.q) {
        filter.Name = new RegExp('^' + params.q, 'i');
        //  filter.State = new RegExp("/^Bih$/i");
    }
    return District.count(filter).exec();
}

module.exports = {
    createNew: createNew,
    updateDistrict: updateDistrict,
    searchById: searchById,
    removeById: removeById,
    getDistricts: getDistricts,
    getTotalDistrictCount: getTotalDistrictCount
};
