/**
 * Image routes.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */
var district = require('./district.controller.js');
//var authentication = require('../authentication/authentication.controller.js');

/**
 * Set image routes.
 *
 * @param {Object} app The express application
 */


function setDistrictRoutes(app) {


    app.route('/api/district')
        .get(district.showDistricts)
        .post(district.createDistrict);

    //.post(authentication.isAuthenticated, image.create)
    app.route('/api/district/:id')
        .get(district.viewById)
        .put(district.updateById);




    /*
        app.route('/api/industry/delete/:id')
        .post(industryCrop.deleteById);*/

    //app.route('/images/:id').delete(authentication.isAuthenticated, image.delete);

}

module.exports = setDistrictRoutes;
