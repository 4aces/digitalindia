'use strict';

/**
 * Module dependencies.
 */

var mongoose = require('mongoose');

var deepPopulate = require('mongoose-deep-populate')(mongoose);

//var industryCrop = require('../industry-crop/industryCrop.model.js');



/**
 * District Crop Schema

 */
var DistrictSchema = new mongoose.Schema({
    Name: {
        type: String,
        required: true
    },
    StateId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'State'
    },
    CropIds: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Crop'
    }],
    Description: {
        type: String,
    }

});

//DistrictIndustrySchema.plugin(deepPopulate);

module.exports = mongoose.model('District', DistrictSchema);
