2/**
 * User controller.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */

var logger = require('mm-node-logger')(module);
var link = require('../utils/links-utils.js');
var dao = require('./district.dao.js');
var Q = require('q');

/**
 * All district Crop.
 *
 * @param {Object} req The request object
 * @param {Object} res The request object
 * @returns {Array} the list of all district Crop 
 * 
 * @api {get} /district? 
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} [page=1]  Optional page with default 1
 * @apiParam {Number} [limit=25]  Optional page limit with default 25
 *
 * @apiSuccess {Object} Crops District wise crop.
 *
 */

function createDistrict(req, res) {
	var districtIndustry = req.body;
	dao.createNew(districtIndustry).then(
	function(response){
		return res.json(response);
	},
	function(error){
		return res.status(500).send(error);
		});
};

function updateById(req,res){
	var id = req.params.id;
	var obj = req.body;
	dao.updateDistrict(id,obj).then(function(response){
		return res.json(response);
	},
	function(error){
	return res.status(500).send(error);
	});
};

function viewById(req,res){
	//var id=rq.params._id;
	return dao.searchById(req.params.id).then(function(response){
		return res.send(response);
	},
	function(error){
	return res.status(500).send(error);
	});
};


function deleteById(req,res){
	//var id=rq.params._id;
	return dao.removeById(req.params.id).then(function(response){
		return res.send(response);
	},
	function(error){
	return res.status(500).send(error);
	});
};


function showDistricts(req,res)
{
	var page = 1;
    var limit = 25;

    var params = {};
    if (req.query) {
        params = req.query;
    }
    if (req.query.page) {
        page = Number(req.query.page);
    }

    if (req.query.limit) {
        limit = Number(req.query.limit);
    }


    var responseData = {};
    var districts = dao.getDistricts(params);
    var totalDistrict = dao.getTotalDistrictCount(params);

    Q.spread([districts, totalDistrict], function(districts, totalDistrict) {
        responseData.districts = districts;
        //
        var prev = link.creatLink(req, 'district', { page: page - 1, limit: limit });
        var next = link.creatLink(req, 'district', { page: page + 1, limit: limit });

        responseData.paging = {};
        if (page > 1) {
            responseData.paging.previous = prev;
        }
        if (page * limit < totalDistrict) {
            responseData.paging.next = next;
        }
        responseData.total = totalDistrict;

        return res.json(responseData);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });

	/*return dao.getIndustries().then(function(response){
		return res.send(response);
	},
	function(error){
	return res.status(500).send(error);
	});	*/
}

module.exports = {
    createDistrict: createDistrict,
    updateById : updateById,
    viewById : viewById,
    deleteById : deleteById,
    showDistricts: showDistricts

};
