/**
 * Image routes.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */
var industryCrop = require('./industryCrop.controller.js');
//var authentication = require('../authentication/authentication.controller.js');

/**
 * Set image routes.
 *
 * @param {Object} app The express application
 */


function setIndustryRoutes(app) {


    app.route('/api/industry')
        .get(industryCrop.getIndustries)
        .post(industryCrop.createIndustry);

    //.post(authentication.isAuthenticated, image.create)
    app.route('/api/industry/:id')
        .get(industryCrop.viewById)
        .put(industryCrop.updateById);




    /*
        app.route('/api/industry/delete/:id')
        .post(industryCrop.deleteById);*/

    //app.route('/images/:id').delete(authentication.isAuthenticated, image.delete);

}

module.exports = setIndustryRoutes;
