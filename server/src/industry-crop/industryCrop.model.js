'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Crop = require('../crop/crop.model.js');

/**
 * District Crop Schema
 */
var IndustryCropSchema = new mongoose.Schema({
    Name: {
		type: String,
		required : true
	},
	
	CropIds : [{ type : mongoose.Schema.Types.ObjectId, ref: 'Crop' }],
	
	Description : {
		type : String,
		required : true
	}
     
});

module.exports = mongoose.model('IndustryCrop', IndustryCropSchema);