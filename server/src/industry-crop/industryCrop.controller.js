/**
 * User controller.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */

var logger = require('mm-node-logger')(module);
var link = require('../utils/links-utils.js');
var dao = require('./industryCrop.dao.js');
var Q = require('q');

/**
 * All district Crop .
 *
 * @param {Object} req The request object
 * @param {Object} res The request object
 * @returns {Array} the list of all district Crop 
 * 
 * @api {get} /district? 
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} [page=1]  Optional page with default 1
 * @apiParam {Number} [limit=25]  Optional page limit with default 25
 *
 * @apiSuccess {Object} Crops District wise crop.
 *
 */

function createIndustry(req, res) {
	var industryCrop = req.body;
	dao.createNew(industryCrop).then(
	function(response){
		return res.json(response);
	},
	function(error){
		return res.status(500).send(error);
		});
};

function updateById(req,res){
	var id = req.params.id;
	var obj = req.body;
	dao.updateIndustry(id,obj).then(function(response){
		return res.json(response);
	},
	function(error){
	return res.status(500).send(error);
	});
};

function viewById(req,res){
	//var id=rq.params._id;
	return dao.searchById(req.params.id).then(function(response){
		return res.send(response);
	},
	function(error){
	return res.status(500).send(error);
	});
};


function deleteById(req,res){
	//var id=rq.params._id;
	return dao.removeById(req.params.id).then(function(response){
		return res.send(response);
	},
	function(error){
	return res.status(500).send(error);
	});
};


function getIndustries(req,res)
{
	var page = 1;
    var limit = 25;

    var params = {};
    if (req.query) {
        params = req.query;
    }
    if (req.query.page) {
        page = Number(req.query.page);
    }

    if (req.query.limit) {
        limit = Number(req.query.limit);
    }


    var responseData = {};
    var industries = dao.getIndustries(params);
    var totalIndustry = dao.getTotalIndustryCount(params);

    Q.spread([industries, totalIndustry], function(industries, totalIndustry) {
        responseData.industries = industries;
        //
        var prev = link.creatLink(req, 'industy', { page: page - 1, limit: limit });
        var next = link.creatLink(req, 'industy', { page: page + 1, limit: limit });

        responseData.paging = {};
        if (page > 1) {
            responseData.paging.previous = prev;
        }
        if (page * limit < totalIndustry) {
            responseData.paging.next = next;
        }
        responseData.total = totalIndustry;

        return res.json(responseData);
    }, function(err) {
        logger.error(err.message);
        return res.status(500).send(err);
    });

	/*return dao.getIndustries().then(function(response){
		return res.send(response);
	},
	function(error){
	return res.status(500).send(error);
	});	*/
}

module.exports = {
    createIndustry: createIndustry,
    updateById : updateById,
    viewById : viewById,
    deleteById : deleteById,
    getIndustries:getIndustries

};
