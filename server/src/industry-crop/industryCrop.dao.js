/**
 * User DAO.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */

var logger = require('mm-node-logger')(module);

var IndustryCrop = require('./industryCrop.model.js');


/**
 * All district Crop .
 *
 * @param {Object} req The request object
 * @param {Object} res The request object
 * @returns {Array} the list of all district Crop 
 * 
 */

function getIndustries(params) {
    var page = 1;
    var limit = 25;
    if (params.page) {
        page = Number(params.page);
    }

    if (params.limit) {
        limit = Number(params.limit);
    }

    var filter = {};
    /**/
    if (params.q) {
        filter.Name = new RegExp('^' + params.q, 'i');
        //  filter.State = new RegExp("/^Bih$/i");
    }
    var order = 'Name';

    if (params.order) {
        order = params.order;
    }
    return IndustryCrop.find(filter, { '__v': 0 }, {
        skip: (limit * (page - 1)),
        limit: limit,
        sort: order
    }).populate('CropIds', { '_id': 0, '__v': 0 }).exec();
}

function createNew(industryCrop) {

    return IndustryCrop.create(industryCrop);

}

function updateIndustry(id, obj) {
    return IndustryCrop.update({ _id: id }, {
        $set: obj
    }).exec();
}

function searchById(id) {
    return IndustryCrop.findById({ _id: id }).exec();

}

function removeById(id) {
    return IndustryCrop.remove({ _id: id }).exec();

}

function getTotalIndustryCount(params) {
    var filter = {};

    if (params.q) {
        filter.Name = new RegExp('^' + params.q, 'i');
        //  filter.State = new RegExp("/^Bih$/i");
    }
    return IndustryCrop.count(filter).exec();
}

module.exports = {
    createNew: createNew,
    updateIndustry: updateIndustry,
    searchById: searchById,
    removeById: removeById,
    getIndustries: getIndustries,
    getTotalIndustryCount: getTotalIndustryCount
};
