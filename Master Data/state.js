var http = require('http');
var fs = require('fs');
var jsonfile = require('jsonfile');

/*fs.readFile('./vocab.nic/state.json', function(err, data) {

    if (err) {
        throw err;
    }

    var obj = JSON.parse(data);

    var states = [];

    obj.states.forEach(function(x) {
        //console.log(x.state);
        var state = {};
        state.id = x.state.state_id;
        state.name = x.state.state_name;
        states.push(state);
    });

    states.forEach(function(x) {
        console.log(x);
    });

    jsonfile.writeFile('states.json', states, 'utf8', (err) => {
        if (err) throw err;
        console.log('It\'s saved!');
    });

});
*/

var states = [];

var districts = [];

jsonfile.readFile('digitalStates.json', readFileCallBack)

function readFileCallBack(err, data) {
    data.forEach(function(x) {
        states.push(x);
    });

    getData();
}



function getData() {
    /*stateCode.forEach(function(x){
        
    });*/

    var state = states.shift();

    //console.log(state);
    //console.log(state.id);

    var options = {
        host: 'vocab.nic.in',
        port: 80,
        path: '/rest.php/district/'+state.id+'/json',
        method: 'GET'
    };
    var response = "";

    http.request(options, function(res) {
        console.log('STATUS: ' + res.statusCode);
        console.log('HEADERS: ' + JSON.stringify(res.headers));
        //res.setEncoding('utf8');
        res.on('data', function(chunk) {
            console.log('BODY: ' + chunk);

            response += chunk;



        });
        res.on('end', () => {
            console.log('No more data in response.');
            createDistrict(state._id, response);
            if (states.length > 0) {
                getData();
            } else {
                 writeDistrictToFile();
            }
        });
    }).end();
}

function createDistrict(stateId, data) {
    console.log("Fillling districts")
    console.log(data);
    data = JSON.parse(data);
    data.districts.forEach(function(x) {

        var dis = {};
        dis.Name = x.district.district_name;
        dis.Id = x.district.district_id;
        dis.StateId = stateId;
        districts.push(dis);
        //
        //
        console.log(dis);
    });

    console.log("********Fillling districts End ***********")
}


function writeDistrictToFile() {
    jsonfile.writeFile('districts.json', districts, 'utf8', (err) => {
        if (err) throw err;
        console.log('It\'s saved!');
    });
}
