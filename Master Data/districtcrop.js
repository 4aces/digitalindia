var http = require('http');
var fs = require('fs');
var jsonfile = require('jsonfile');



var districtsCrop = [];
var mappedDistrictCrop = [];
jsonfile.readFile('districtcrop.json', readFileCallBack)

function readFileCallBack(err, data) {

    districtsCrop = data;
    start();
}

function start() {
    console.log("Mapping Start");
   mappedDistrictCrop=districtsCrop.map(function(x) {
        //console.log(x.Crops);
        
        x.CropIds = remap(x.CropIds);
        x.StateId = findStateIdByName(x.StateId);
       // console.log(x.Crops);
       return x;
    });
    console.log("Mapping done");
    writeToFile();
}

function remap(arr) {
    var result = arr.map(function(x) {
        //console.log(x);
        //console.log(findCropIdByName(x));
       return findCropIdByName(x);
        //console.log(x);
    })
    return result;
}


function writeToFile() {
    jsonfile.writeFile('districtCropMapped.json', mappedDistrictCrop, 'utf8', (err) => {
        if (err) throw err;
        console.log('It\'s saved!');
    });
}

function findCropIdByName(crop) {
    var id = "";
    for (var i = 0; i < crops.length; i++) {
        if (crops[i].value === crop) {
            id = crops[i].key;
            break;
        }
    }

    return id;
}

function findStateIdByName(state) {
    var id = "";
    for (var i = 0; i < states.length; i++) {
        if (states[i].value === state) {
            id = states[i].key;
            break;
        }
    }

    return id;
}

var states = [
  {
    "key": "579b943f44c0110f9a315b57",
    "value": "JHARKHAND"
  },
  {
    "key": "579b943f44c0110f9a315b58",
    "value": "UTTARANCHAL"
  },
  {
    "key": "579b943f44c0110f9a315b59",
    "value": "CHHATTISGARH"
  },
  {
    "key": "579b943f44c0110f9a315b5a",
    "value": "A & N ISLANDS"
  },
  {
    "key": "579b943f44c0110f9a315b5b",
    "value": "UTTAR PRADESH"
  },
  {
    "key": "579b943f44c0110f9a315b5c",
    "value": "PONDICHERRY"
  },
  {
    "key": "579b943f44c0110f9a315b5d",
    "value": "NAGALAND"
  },
  {
    "key": "579b943f44c0110f9a315b5e",
    "value": "MAHARASHTRA"
  },
  {
    "key": "579b943f44c0110f9a315b5f",
    "value": "GUJARAT"
  },
  {
    "key": "579b943f44c0110f9a315b60",
    "value": "ASSAM"
  },
  {
    "key": "579b943f44c0110f9a315b61",
    "value": "GOA"
  },
  {
    "key": "579b943f44c0110f9a315b62",
    "value": "MEGHALYA"
  },
  {
    "key": "579b943f44c0110f9a315b63",
    "value": "ORISSA"
  },
  {
    "key": "579b943f44c0110f9a315b64",
    "value": "DELHI"
  },
  {
    "key": "579b943f44c0110f9a315b65",
    "value": "ANDHRA PRADESH"
  },
  {
    "key": "579b943f44c0110f9a315b66",
    "value": "TRIPURA"
  },
  {
    "key": "579b943f44c0110f9a315b67",
    "value": "HARYANA"
  },
  {
    "key": "579b943f44c0110f9a315b68",
    "value": "RAJASTHAN"
  },
  {
    "key": "579b943f44c0110f9a315b69",
    "value": "PUNJAB"
  },
  {
    "key": "579b943f44c0110f9a315b6a",
    "value": "WEST BENGAL"
  },
  {
    "key": "579b943f44c0110f9a315b6b",
    "value": "CHANDIGARH"
  },
  {
    "key": "579b943f44c0110f9a315b6c",
    "value": "ARUNACHAL PRADESH"
  },
  {
    "key": "579b943f44c0110f9a315b6d",
    "value": "TAMIL NADU"
  },
  {
    "key": "579b943f44c0110f9a315b6e",
    "value": "BIHAR"
  },
  {
    "key": "579b943f44c0110f9a315b6f",
    "value": "KERALA"
  },
  {
    "key": "579b943f44c0110f9a315b70",
    "value": "HIMACHAL PRADESH"
  },
  {
    "key": "579b943f44c0110f9a315b71",
    "value": "SIKKIM"
  },
  {
    "key": "579b943f44c0110f9a315b72",
    "value": "MIZORAM"
  },
  {
    "key": "579b943f44c0110f9a315b73",
    "value": "KARNATAKA"
  },
  {
    "key": "579b943f44c0110f9a315b74",
    "value": "D & N HAVELI"
  },
  {
    "key": "579b943f44c0110f9a315b75",
    "value": "JAMMU & KASHMIR"
  },
  {
    "key": "579b943f44c0110f9a315b76",
    "value": "MANIPUR"
  },
  {
    "key": "579b943f44c0110f9a315b77",
    "value": "MADHYA PRADESH"
  }
];

var crops = [{
    "key": "578b9f61b746958023853016",
    "value": "Other Dry Fruit"
}, {
    "key": "578b9f61b746958023853017",
    "value": "Litchi"
}, {
    "key": "578b9f61b746958023853019",
    "value": "Yam"
}, {
    "key": "578b9f61b74695802385301a",
    "value": "Pome Granet"
}, {
    "key": "578b9f61b746958023853018",
    "value": "Plums"
}, {
    "key": "578b9f61b74695802385301c",
    "value": "Lab-Lab"
}, {
    "key": "578b9f61b74695802385301d",
    "value": "Beet Root"
}, {
    "key": "578b9f61b74695802385301b",
    "value": "Other Citrus Fruit"
}, {
    "key": "578b9f61b74695802385301f",
    "value": "Cauliflower"
}, {
    "key": "578b9f61b746958023853020",
    "value": "Snak Guard"
}, {
    "key": "578b9f61b746958023853021",
    "value": "Cashewnut Raw"
}, {
    "key": "578b9f61b74695802385301e",
    "value": "Ash Gourd"
}, {
    "key": "578b9f61b746958023853023",
    "value": "Carrot"
}, {
    "key": "578b9f61b746958023853024",
    "value": "Peas  (vegetable)"
}, {
    "key": "578b9f61b746958023853025",
    "value": "Grapes"
}, {
    "key": "578b9f61b746958023853026",
    "value": "Tomato"
}, {
    "key": "578b9f61b746958023853027",
    "value": "Bitter Gourd"
}, {
    "key": "578b9f61b746958023853028",
    "value": "Other Vegetables"
}, {
    "key": "578b9f61b746958023853029",
    "value": "Arcanut (Processed)"
}, {
    "key": "578b9f61b74695802385302a",
    "value": "Sweet potato"
}, {
    "key": "578b9f61b746958023853022",
    "value": "Atcanut (Raw)"
}, {
    "key": "578b9f61b74695802385302b",
    "value": "other fibres"
}, {
    "key": "578b9f61b74695802385302c",
    "value": "Peach"
}, {
    "key": "578b9f61b74695802385302d",
    "value": "Orange"
}, {
    "key": "578b9f61b74695802385302e",
    "value": "Jack Fruit"
}, {
    "key": "578b9f61b74695802385302f",
    "value": "Oilseeds total"
}, {
    "key": "578b9f61b746958023853030",
    "value": "Moth"
}, {
    "key": "578b9f61b746958023853031",
    "value": "Citrus Fruit"
}, {
    "key": "578b9f61b746958023853032",
    "value": "Coffee"
}, {
    "key": "578b9f61b746958023853033",
    "value": "Cabbage"
}, {
    "key": "578b9f61b746958023853034",
    "value": "Beans & Mutter(Vegetable)"
}, {
    "key": "578b9f61b746958023853035",
    "value": "Varagu"
}, {
    "key": "578b9f61b746958023853036",
    "value": "Rubber"
}, {
    "key": "578b9f61b746958023853037",
    "value": "Safflower"
}, {
    "key": "578b9f61b746958023853038",
    "value": "Jute & mesta"
}, {
    "key": "578b9f61b746958023853039",
    "value": "Mesta"
}, {
    "key": "578b9f61b74695802385303a",
    "value": "Khesari"
}, {
    "key": "578b9f61b74695802385303b",
    "value": "other oilseeds"
}, {
    "key": "578b9f61b74695802385303c",
    "value": "Sannhamp"
}, {
    "key": "578b9f61b74695802385303d",
    "value": "Black pepper"
}, {
    "key": "578b9f61b74695802385303e",
    "value": "Guar seed"
}, {
    "key": "578b9f61b74695802385303f",
    "value": "Pulses total"
}, {
    "key": "578b9f61b746958023853040",
    "value": "Water Melon"
}, {
    "key": "578b9f61b746958023853041",
    "value": "Soyabean"
}, {
    "key": "578b9f61b746958023853042",
    "value": "Dry ginger"
}, {
    "key": "578b9f61b746958023853043",
    "value": "Pineapple"
}, {
    "key": "578b9f62b746958023853044",
    "value": "Other Cereals & Millets"
}, {
    "key": "578b9f62b746958023853045",
    "value": "Cardamom"
}, {
    "key": "578b9f62b746958023853046",
    "value": "Kapas"
}, {
    "key": "578b9f62b746958023853047",
    "value": "Niger seed"
}, {
    "key": "578b9f62b746958023853048",
    "value": "other misc. pulses"
}, {
    "key": "578b9f62b746958023853049",
    "value": "Small millets"
}, {
    "key": "578b9f62b74695802385304a",
    "value": "Maize"
}, {
    "key": "578b9f62b74695802385304b",
    "value": "Rapeseed &Mustard"
}, {
    "key": "578b9f62b74695802385304c",
    "value": "Urad"
}, {
    "key": "578b9f62b74695802385304d",
    "value": "Pear"
}, {
    "key": "578b9f62b74695802385304e",
    "value": "Ribed Guard"
}, {
    "key": "578b9f62b74695802385304f",
    "value": "Masoor"
}, {
    "key": "578b9f62b746958023853050",
    "value": "Rice"
}, {
    "key": "578b9f62b746958023853051",
    "value": "Gram"
}, {
    "key": "578b9f62b746958023853052",
    "value": "Banana"
}, {
    "key": "578b9f62b746958023853053",
    "value": "Coriander"
}, {
    "key": "578b9f62b746958023853054",
    "value": "Cucumber"
}, {
    "key": "578b9f62b746958023853055",
    "value": "Tapioca"
}, {
    "key": "578b9f62b746958023853056",
    "value": "Korra"
}, {
    "key": "578b9f62b746958023853057",
    "value": "Wheat"
}, {
    "key": "578b9f62b746958023853058",
    "value": "Jute"
}, {
    "key": "578b9f62b746958023853059",
    "value": "Garlic"
}, {
    "key": "578b9f62b74695802385305a",
    "value": "Apple"
}, {
    "key": "578b9f62b74695802385305b",
    "value": "Coconut ('000 Nuts)"
}, {
    "key": "578b9f62b74695802385305c",
    "value": "Cotton(lint)"
}, {
    "key": "578b9f62b74695802385305d",
    "value": "Turmeric"
}, {
    "key": "578b9f62b74695802385305e",
    "value": "Sesamum"
}, {
    "key": "578b9f62b74695802385305f",
    "value": "Onion"
}, {
    "key": "578b9f62b746958023853060",
    "value": "Potato"
}, {
    "key": "578b9f62b746958023853061",
    "value": "Barley"
}, {
    "key": "578b9f62b746958023853062",
    "value": "Samai"
}, {
    "key": "578b9f62b746958023853063",
    "value": "Cashewnut Processed"
}, {
    "key": "578b9f62b746958023853064",
    "value": "Redish"
}, {
    "key": "578b9f62b746958023853065",
    "value": "Pome Fruit"
}, {
    "key": "578b9f62b746958023853066",
    "value": "Other Fresh Fruits"
}, {
    "key": "578b9f62b746958023853067",
    "value": "Mango"
}, {
    "key": "578b9f62b746958023853068",
    "value": "Peas & beans (Pulses)"
}, {
    "key": "578b9f62b746958023853069",
    "value": "Tobacco"
}, {
    "key": "578b9f62b74695802385306a",
    "value": "Pump Kin"
}, {
    "key": "578b9f62b74695802385306b",
    "value": "Other  Rabi pulses"
}, {
    "key": "578b9f62b74695802385306c",
    "value": "Bottle Gourd"
}, {
    "key": "578b9f62b74695802385306d",
    "value": "Drum Stick"
}, {
    "key": "578b9f62b74695802385306e",
    "value": "Cond-spcs other"
}, {
    "key": "578b9f62b74695802385306f",
    "value": "Arhar"
}, {
    "key": "578b9f62b746958023853070",
    "value": "Horse-gram"
}, {
    "key": "578b9f62b746958023853071",
    "value": "Sunflower"
}, {
    "key": "578b9f62b746958023853072",
    "value": "Linseed"
}, {
    "key": "578b9f62b746958023853073",
    "value": "Ber"
}, {
    "key": "578b9f62b746958023853074",
    "value": "Bajra"
}, {
    "key": "578b9f62b746958023853075",
    "value": "Papaya"
}, {
    "key": "578b9f62b746958023853076",
    "value": "Groundnut"
}, {
    "key": "578b9f62b746958023853077",
    "value": "Turnip"
}, {
    "key": "578b9f62b746958023853078",
    "value": "Sugarcane"
}, {
    "key": "578b9f62b746958023853079",
    "value": "Cashewnut"
}, {
    "key": "578b9f62b74695802385307a",
    "value": "Dry chillies"
}, {
    "key": "578b9f62b74695802385307b",
    "value": "Ragi"
}, {
    "key": "578b9f62b74695802385307c",
    "value": "Brinjal"
}, {
    "key": "578b9f62b74695802385307d",
    "value": "Jowar"
}, {
    "key": "578b9f62b74695802385307e",
    "value": "Other Kharif pulses"
}, {
    "key": "578b9f62b74695802385307f",
    "value": "Castor seed"
}, {
    "key": "578b9f62b746958023853080",
    "value": "Bhindi"
}, {
    "key": "578b9f62b746958023853081",
    "value": "Tea"
}, {
    "key": "578b9f62b746958023853082",
    "value": "Moong"
}, {
    "key": "578b9f62b746958023853083",
    "value": "Arecanut"
}, {
    "key": "578b9f62b746958023853084",
    "value": "Total foodgrain"
}]
