var http = require('http');
var fs = require('fs');
var jsonfile = require('jsonfile');



var state = [];
var output = [];
jsonfile.readFile('state_input.json', readFileCallBack)

function readFileCallBack(err, data) {

    state = data;
    start();
}

function start() {
    console.log("Mapping Start");
    output = state.map(function(x) {
        return x;
    });
    console.log("Mapping done");
    writeToFile();
}

function writeToFile() {
    jsonfile.writeFile('state_output.json', output, 'utf8', (err) => {
        if (err) throw err;
        console.log('It\'s saved!');
    });
}

function findCropIdByName(crop) {
    var id = "";
    for (var i = 0; i < crops.length; i++) {
        if (crops[i].value === crop) {
            id = crops[i].key;
            break;
        }
    }

    return id;
}
