/**
 * Image model.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var config = {};
config.mongodb = {
    dbURI: process.env.MONGODB_URI || process.env.MONGOLAB_URI || "mongodb://127.0.0.1:27017/digitalindia",
    dbOptions: { "user": "", "pass": "" }
};

mongoose.connect(config.mongodb.dbURI, config.mongodb.dbOptions);
mongoose.Promise = require('q').Promise;
/**
 * District Crop Schema
 */
var ProductionSchema = new mongoose.Schema({
    Year: {
        type: Number
    },
    StateId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'State',

        required: true
    },
    DistrictId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'District',

        required: true
    },
    CropId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Crop',

        required: true
    },
    Season: {
        type: String,
        trim: true,
        required: true
    },
    Area: {
        type: Number
    },
    Production: {
        type: Number
    }
});

module.exports = mongoose.model('Production', ProductionSchema);
