
var jsonfile = require('jsonfile');

//Converter Class
var Converter = require("csvtojson").Converter;
var converter = new Converter({});

//end_parsed will be emitted once parsing finished
converter.on("end_parsed", function (jsonArray) {
   console.log(jsonArray); //here is your result jsonarray
   	jsonfile.writeFile('production_input.json', jsonArray, 'utf8', (err) => {
        if (err) throw err;
        console.log('It\'s saved!');
    });
});

//read from file
require("fs").createReadStream("./apy.csv").pipe(converter)
