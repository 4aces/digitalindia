var fs = require('fs');
var jsonfile = require('jsonfile');


var task = [];

var states = [];
var districts = [];
var crops = [];
var production_input = [];
var production_output = [];

var task_done=0;

var readState = function() {
    jsonfile.readFile('state.json', function(err, data) {
        if (err) {
            console.log(err);
            next(true);
        } else {
            states = data;
            next(false);
        }
    });
}

task.push(readState);

var readDistrict = function() {
    jsonfile.readFile('district.json', function(err, data) {
        if (err) {
            console.log(err);
            next(true);
        } else {
            district = data;
            next(false);
        }
    });
}

task.push(readDistrict);

var readCrop = function() {
    jsonfile.readFile('crop.json', function(err, data) {
        if (err) {
            console.log(err);
            next(true);
        } else {
            crops = data;
            next(false);
        }
    });
}

task.push(readCrop);

var readProduction = function() {
    jsonfile.readFile('production_input.json', function(err, data) {
        if (err) {
            console.log(err);
            next(true);
        } else {
            production_input = data;
            next(false);
        }
    });
}

task.push(readProduction);
(function start() {

	console.log('File Read start')
    for (var i = 0; i < task.length; i++) {
        task[i]();
    }
})();

function next(result)
{
	if(!result)
	{
			task_done++;

			console.log('File'+task_done+'/'+task.length+' finished');
			if(task_done== task.length)
			{
				console.log('File Reading Completed');
				loadDone();
			}
	}
	else{
		throw new Error('oops something went wrong');
	}
}


function loadDone()
{
	console.log('****mapping start****');
	for(var i =0 ; i< production_input.length; i++)
	{
		production_input[i].StateId = find(states,production_input[i].State);
		production_input[i].DistrictId = find(district,production_input[i].District);
		production_input[i].CropId = find(crops,production_input[i].Crop);

		delete production_input[i].State;
		delete production_input[i].District;
		delete production_input[i].Crop;
	
		console.log((i+0)+'/'+production_input.length+' finished');		
		
		
	}
	console.log('****mapping done****');
	writeToFile();
	
}

function find(arr,value)
{
	for(var i =0; i< arr.length; i++)
	{
		if(arr[i].value == value)		{
			return arr[i].key;
		}
	}
}


function writeToFile()
{
	jsonfile.writeFile('production_output.json', production_input, 'utf8', (err) => {
        if (err) throw err;
        console.log('It\'s saved!');
    });
}