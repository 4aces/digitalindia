/**
 * Image model.
 *
 * @author    Naveen Sharma {@link http://_naveen}
 * @copyright Copyright (c) 2015, Naveen Sharma
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var config = {};
config.mongodb = {
    dbURI: process.env.MONGODB_URI || process.env.MONGOLAB_URI || "mongodb://127.0.0.1:27017/digitalindia",
    dbOptions: { "user": "", "pass": "" }
};

mongoose.connect(config.mongodb.dbURI, config.mongodb.dbOptions);
mongoose.Promise = require('q').Promise;
/**
 * District Crop Schema
 */
var DistrictSchema = new mongoose.Schema({
    Name: {
        type: String,
        required: true
    },
    StateId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'State'
    },
    CropIds: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Crop'
    }],
    Description: {
        type: String,
    }

});

//DistrictIndustrySchema.plugin(deepPopulate);

module.exports = mongoose.model('District', DistrictSchema);