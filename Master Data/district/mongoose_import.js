var District = require('./mongooseModel');


var fs = require('fs');
var jsonfile = require('jsonfile');

var production_output = [];

var readProduction = function() {
    console.log('reading.....')
    jsonfile.readFile('district-crop.json', function(err, data) {
        if (err) {
            console.log(err);

        } else {
            production_output = data;
            console.log('reading done');
            start();
        }
    });
}

readProduction();

var chunk_size = 100;
var init = 0;


function start() {
    /*var i, j, temparray ;
    for (i = 0, j = production_output.length; i < j; i += chunk) {
        temparray = production_output.slice(i, i + chunk);
        // do whatever
    }*/
    console.log(init +"-" +(init+chunk_size) +"/"+production_output.length);
    temparray = production_output.slice(init, init + chunk_size);

    insert(temparray);
}


function insert(data) {
    District.create(data).then(function(res) {
        if (init < production_output.length) {
            init += chunk_size;
            start();
        } else {
            console.log('Done');
        }
    }, function(err) {
        console.log(err);
    })
}
