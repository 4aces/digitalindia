# README #



### How do I get set up? ###

#### Prerequisite software: ####
    monogodb >= 3.2.0
    node >= 6.0.0

#### Set up: ####
##### 1. restore database #####
    $sudo mongorestore --db digitalindia --drop 'from your root path open project folder'/digitalindia/mongobackups/08-20-16/digitalindia/

##### 2. install http-server #####
    $sudo npm install http-server -g 

#### Run application : ####

##### 1. Server #####
**a) open project folder **

**b) open server folder and run the following commands **

    $cd server
    $npm install
    $npm start
*Note : server will be running on port no 4000*  
    
##### 2. Client #####
**a) open project folder**

**b) open client folder and run the following commands**

    $http-server
*Note : client will be running on port no 8080*

**3. open http://localhost:8080 in browser**
  
     





